package me.joeyleavell.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import me.joeyleavell.Game;

public class DesktopLauncher
{
	public static void main(String[] arg)
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Game.VIRTUAL_WIDTH;
		config.height = Game.VIRTUAL_HEIGHT;
		new LwjglApplication(new Game(), config);
	}
}