package me.joeyleavell;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import me.joeyleavell.world.World;

public class TextureUtil
{

	public static TextureRegion[] split(TextureRegion texture, int cellWidth, int cellHeight)
	{
		int tilesWide = texture.getRegionWidth() / cellWidth;
		int tilesTall = texture.getRegionHeight() / cellHeight;
		TextureRegion[] result = new TextureRegion[tilesWide * tilesTall];

		for (int x = 0; x < tilesWide; x++)
			for (int y = 0; y < tilesTall; y++)
				result[y * tilesWide + x] = new TextureRegion(texture, x * cellWidth, (texture.getRegionHeight() - cellHeight) - y * cellHeight, cellWidth, cellHeight);

		return result;
	}

	public static TextureRegion[] split(TextureRegion texture)
	{
		return split(texture, World.TILE_SIZE, World.TILE_SIZE);
	}

	public static TextureRegion fixBleeding(TextureRegion region)
	{
		float x = region.getRegionX();
		float y = region.getRegionY();
		float width = region.getRegionWidth();
		float height = region.getRegionHeight();
		float invTexWidth = 1f / region.getTexture().getWidth();
		float invTexHeight = 1f / region.getTexture().getHeight();
		region.setRegion((x + .05f) * invTexWidth, (y + .05f) * invTexHeight, (x + width - .05f) * invTexWidth, (y + height - .05f) * invTexHeight);
		return region;
	}

}
