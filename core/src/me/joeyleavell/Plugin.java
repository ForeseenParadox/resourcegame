package me.joeyleavell;

import java.util.Properties;

public abstract class Plugin
{

	private String name;
	private String description;
	private String mainClass;
	private String version;
	private String author;
	private String website;
	private String icon;
	private Game game;

	public Plugin()
	{
	}

	public void load(Properties modProperties, Game game)
	{
		mainClass = modProperties.getProperty("main");
		name = modProperties.getProperty("name");
		description = modProperties.getProperty("description");
		version = modProperties.getProperty("version");
		author = modProperties.getProperty("author");
		website = modProperties.getProperty("website");
		icon = modProperties.getProperty("icon");
		this.game = game;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public String getMainClass()
	{
		return mainClass;
	}

	public String getVersion()
	{
		return version;
	}

	public String getAuthor()
	{
		return author;
	}

	public String getWebsite()
	{
		return website;
	}

	public String getIcon()
	{
		return icon;
	}

	public Game getGame()
	{
		return game;
	}

	public abstract void onLoad();

	public abstract void onDispose();
}
