package me.joeyleavell;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AudioManager
{

	private String currentMusic;

	private String dayMusic;
	private String nightMusic;

	public AudioManager()
	{
		dayMusic = Sounds.MUSIC_THEME_1;
		nightMusic = Sounds.MUSIC_THEME_6;
	}

	public String getDayMusic()
	{
		return dayMusic;
	}

	public String getNightMusic()
	{
		return nightMusic;
	}

	public void playDayMusic()
	{
		if (!currentMusic.equals(dayMusic))
			setCurrentMusic(dayMusic, true);
	}

	public void playNightMusic()
	{
		if (!currentMusic.equals(nightMusic))
			setCurrentMusic(nightMusic, true);
	}

	public String getCurrentMusic()
	{
		return currentMusic;
	}

	public void setCurrentMusic(String music, boolean loop)
	{
		if (currentMusic != null)
			Game.getInstance().getAssetManager().get(currentMusic, Music.class).stop();

		currentMusic = music;
		Music m = Game.getInstance().getAssetManager().get(music, Music.class);

		m.play();
		m.setLooping(loop);
	}

	public void playSfx(String sound)
	{
		Game.getInstance().getAssetManager().get(sound, Sound.class).play(Game.getInstance().getSettings().getSfxVolume());
	}

}
