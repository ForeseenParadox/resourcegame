package me.joeyleavell;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.Hinting;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import me.joeyleavell.building.BuildingDefinition;
import me.joeyleavell.building.BuildingLight;
import me.joeyleavell.command.CommandManager;
import me.joeyleavell.entity.EntityFactory;
import me.joeyleavell.event.EventManager;
import me.joeyleavell.states.StateCreateGame;
import me.joeyleavell.states.StateLoadGame;
import me.joeyleavell.states.StateMainMenu;
import me.joeyleavell.states.StateManager;
import me.joeyleavell.states.StateMods;
import me.joeyleavell.states.StatePlay;
import me.joeyleavell.states.options.StateOptions;
import me.joeyleavell.world.QuadraticAttenuation;
import me.joeyleavell.world.biome.Biome;
import me.joeyleavell.world.gen.DefaultWorldGenerator;
import me.joeyleavell.world.tile.Tiles;

public class Game implements ApplicationListener
{

	// 4615

	public static boolean debugEnabled = false;

	public static final int VIRTUAL_WIDTH = 16 * 60;
	public static final int VIRTUAL_HEIGHT = 9 * 60;

	private static Game instance;

	public static final String TILE_SHEET = "Textures/Game/Tile_Overworld.png";
	public static final String ENTITY_SHEET = "Textures/Game/SP_Entities.png";
	public static final String PARTICLE_SHEET = "Textures/Game/SP_Particles.png";
	public static final String MENU_BG_LOC = "Textures/GUI/BG_Title.png";
	public static final String GUI_ATLAS = "Skins/Custom/custom.atlas";
	public static final String PLUGIN_FOLDER = "./plugins";

	public static final int STATE_CREATE_GAME = 0x00;
	public static final int STATE_LOAD_GAME = 0x01;
	public static final int STATE_MAIN_MENU = 0x02;
	public static final int STATE_PLAY = 0x03;
	public static final int STATE_PLUGINS = 0x04;
	public static final int STATE_OPTIONS = 0x05;

	private StateManager screenManager;
	private AssetManager assetManager;
	private ModManager modManager;
	private CommandManager cmdManager;
	private TaskManager taskManager;
	private GameRegistry gameRegistry;
	private EventManager eventManager;
	private AudioManager audioManager;
	private ShapeRenderer shapeRenderer;
	private GameSettings settings;
	private Viewport guiViewport;

	private Skin gameSkin;

	public Game()
	{
		instance = this;
	}

	public static Texture getTileSheet()
	{
		return getInstance().getTexture(TILE_SHEET);
	}

	public static Texture getEntitySheet()
	{
		return getInstance().getTexture(ENTITY_SHEET);
	}

	public static Texture getParticleSheet()
	{
		return getInstance().getTexture(PARTICLE_SHEET);
	}

	public static Texture getMenuBG()
	{
		return getInstance().getTexture(MENU_BG_LOC);
	}

	public static Skin getSkin()
	{
		return getInstance().gameSkin;
	}

	public Texture getTexture(String fileLoc)
	{
		return assetManager.get(fileLoc, Texture.class);
	}

	public static StateManager getScreenManager()
	{
		return getInstance().screenManager;
	}

	public CommandManager getCommandManager()
	{
		return cmdManager;
	}

	public TaskManager getTaskManager()
	{
		return taskManager;
	}

	public GameRegistry getGameRegistry()
	{
		return gameRegistry;
	}

	public AssetManager getAssetManager()
	{
		return assetManager;
	}

	public EventManager getEventManager()
	{
		return eventManager;
	}

	public ShapeRenderer getShapeRenderer()
	{
		return shapeRenderer;
	}

	public AudioManager getAudioManager()
	{
		return audioManager;
	}

	public GameSettings getSettings()
	{
		return settings;
	}

	public ModManager getModManager()
	{
		return modManager;
	}

	public Viewport getGuiViewport()
	{
		return guiViewport;
	}

	@Override
	public void create()
	{
		assetManager = new AssetManager();

		// load textures
		assetManager.load(TILE_SHEET, Texture.class);
		assetManager.load(ENTITY_SHEET, Texture.class);
		assetManager.load(PARTICLE_SHEET, Texture.class);
		assetManager.load(MENU_BG_LOC, Texture.class);
		assetManager.load(GUI_ATLAS, TextureAtlas.class);

		// load sfx
		assetManager.load(Sounds.MUSIC_THEME_1, Music.class);
		assetManager.load(Sounds.MUSIC_THEME_6, Music.class);

		assetManager.finishLoading();

		// Load custom skin font.
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/PressStart2P.ttf"));
		FreeTypeFontParameter param = new FreeTypeFontParameter();
		param.size = 12;
		param.hinting = Hinting.None;
		BitmapFont fnt = gen.generateFont(param);

		// Load all skin features in.
		gameSkin = new Skin();
		gameSkin.add("default-font", fnt);
		gameSkin.addRegions(assetManager.get(GUI_ATLAS, TextureAtlas.class));
		gameSkin.load(Gdx.files.internal("Skins/Custom/custom.json"));

		// init audio manager
		audioManager = new AudioManager();

		// init game registry
		gameRegistry = new GameRegistry();
		gameRegistry.registerTile(Tiles.AIR);
		gameRegistry.registerTile(Tiles.LIGHT_GRASS);
		gameRegistry.registerTile(Tiles.DARK_GRASS);
		gameRegistry.registerTile(Tiles.DIRT);
		gameRegistry.registerTile(Tiles.SAND);
		gameRegistry.registerTile(Tiles.LIGHT_STONE);
		gameRegistry.registerTile(Tiles.DARK_STONE);
		gameRegistry.registerTile(Tiles.SNOW);
		gameRegistry.registerTile(Tiles.WATER);
		gameRegistry.registerTile(Tiles.CAMPFIRE);
		gameRegistry.registerTile(Tiles.SMALL_HOUSE);
		gameRegistry.registerTile(Tiles.BIG_HOUSE);
		gameRegistry.registerTile(Tiles.MAGIC_TOWER);
		gameRegistry.registerTile(Tiles.BARRACKS);
		gameRegistry.registerTile(Tiles.FLAG_POLE_BOTTOM);
		gameRegistry.registerTile(Tiles.FLAG_POLE_MIDDLE);
		gameRegistry.registerTile(Tiles.FLAG_POLE_TOP);
		gameRegistry.registerTile(Tiles.WHEAT);
		gameRegistry.registerTile(Tiles.BOULDER_1);
		gameRegistry.registerTile(Tiles.BOULDER_2);
		gameRegistry.registerTile(Tiles.TREE_1);
		gameRegistry.registerTile(Tiles.TREE_2);
		gameRegistry.registerTile(Tiles.SHRUB_1);
		gameRegistry.registerTile(Tiles.SHRUB_2);
		gameRegistry.registerTile(Tiles.CACTUS_1);
		gameRegistry.registerTile(Tiles.CACTUS_2);
		gameRegistry.registerTile(Tiles.CACTUS_3);
		gameRegistry.registerTile(Tiles.CACTUS_4);

		gameRegistry.registerBiome(Biome.DESERT);
		gameRegistry.registerBiome(Biome.FOREST);
		gameRegistry.registerBiome(Biome.GRASSLAND);
		gameRegistry.registerBiome(Biome.MOUNTAIN);
		gameRegistry.registerBiome(Biome.TUNDRA);
		gameRegistry.registerBiome(Biome.OCEAN);

		gameRegistry.registerBuilding(new BuildingDefinition(Tiles.CAMPFIRE, Textures.CAMPFIRE_1, 1, 1, 50).setLight(new BuildingLight(1, new QuadraticAttenuation(.02f, 0, 0))).setRegistryName("campfire"));
		gameRegistry.registerBuilding(new BuildingDefinition(Tiles.SMALL_HOUSE, Textures.SMALL_HOUSE, 2, 2, 50).setRegistryName("small_house").addEntitySpawn(EntityFactory.VILLAGER, 3).setBuildingEntityId(EntityFactory.HOUSE));
		gameRegistry.registerBuilding(new BuildingDefinition(Tiles.BIG_HOUSE, Textures.BIG_HOUSE, 3, 2, 50).setRegistryName("big_house").addEntitySpawn(EntityFactory.VILLAGER, 6).setBuildingEntityId(EntityFactory.HOUSE));
		gameRegistry.registerBuilding(new BuildingDefinition(Tiles.MAGIC_TOWER, Textures.MAGIC_TOWER, 2, 3, 50).setRegistryName("magic_tower"));
		gameRegistry.registerBuilding(new BuildingDefinition(Tiles.BARRACKS, Textures.BARRACKS, 3, 2, 50).setRegistryName("barracks"));

		BuildingDefinition flag = new BuildingDefinition(Textures.FLAG_POLE_1, 1, 3, 50).setRegistryName("flag");
		flag.setTile(0, 0, Tiles.FLAG_POLE_BOTTOM, 0);
		flag.setTile(0, 1, Tiles.FLAG_POLE_MIDDLE, 0);
		flag.setTile(0, 2, Tiles.FLAG_POLE_TOP, 0);

		gameRegistry.registerBuilding(flag);
		gameRegistry.registerBuilding(new BuildingDefinition(Tiles.WHEAT, Textures.FARM_1, 1, 1, 50).setRegistryName("wheat"));

		gameRegistry.registerWorldGenerator(new DefaultWorldGenerator());

		// init game settings
		settings = new GameSettings();

		// initialize event manager
		eventManager = new EventManager();

		// init shape renderer
		shapeRenderer = new ShapeRenderer();

		// init viewport
		guiViewport = new StretchViewport(VIRTUAL_WIDTH, VIRTUAL_HEIGHT);

		// initialize screens
		screenManager = new StateManager();
		screenManager.registerState(STATE_MAIN_MENU, new StateMainMenu());
		screenManager.registerState(STATE_CREATE_GAME, new StateCreateGame());
		screenManager.registerState(STATE_LOAD_GAME, new StateLoadGame());
		screenManager.registerState(STATE_PLAY, new StatePlay());
		screenManager.registerState(STATE_PLUGINS, new StateMods());
		screenManager.registerState(STATE_OPTIONS, new StateOptions(settings));
		screenManager.pushState(STATE_MAIN_MENU);

		// init command manager
		cmdManager = new CommandManager();

		// init task manager
		taskManager = new TaskManager();

		// init mod manager
		modManager = new ModManager(PLUGIN_FOLDER);
		modManager.loadMods(this);

		audioManager.setCurrentMusic(Sounds.MUSIC_THEME_1, true);

	}

	@Override
	public void resize(int width, int height)
	{
		guiViewport.update(width, height);
		screenManager.resizeCurrent(width, height);
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// update task manager
		taskManager.updateAll();

		screenManager.renderCurrent(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void dispose()
	{
		screenManager.disposeAll();
		assetManager.dispose();
		gameSkin.dispose();
		modManager.disposeMods();
	}

	public static Game getInstance()
	{
		return instance;
	}

}
