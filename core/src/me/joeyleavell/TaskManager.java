package me.joeyleavell;

import java.util.ArrayList;
import java.util.List;

public class TaskManager
{

	private List<SyncTask> tasks;

	public TaskManager()
	{
		tasks = new ArrayList<SyncTask>();
	}

	public List<SyncTask> getTasks()
	{
		return tasks;
	}

	public void registerTask(SyncTask task)
	{
		tasks.add(task);
	}

	public void unregisterTask(SyncTask task)
	{
		tasks.remove(task);
	}

	public void unregisterAll()
	{
		tasks.clear();
	}

	public void updateAll()
	{
		for (SyncTask task : tasks)
			if (task.ready())
				task.invoke();
	}

}
