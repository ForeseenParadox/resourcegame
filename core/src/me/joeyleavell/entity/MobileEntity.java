package me.joeyleavell.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;

public class MobileEntity extends Entity
{

	private float x, y;
	private float w, h;

	private Animation anim;
	private float deltaSum;
	private boolean pauseAnimation;

	public MobileEntity(World world, int id)
	{
		super(world, id);
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public void setPosition(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Location getChunkLocation()
	{
		return getWorld().getTileChunkLocation((int) (getX() / World.TILE_SIZE), (int) (getY() / World.TILE_SIZE));
	}

	public float getWidth()
	{
		return w;
	}

	public float getHeight()
	{
		return h;
	}

	public Animation getAnimation()
	{
		return anim;
	}

	public void setAnimationPaused(boolean pause)
	{
		this.pauseAnimation = pause;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public void setWidth(float w)
	{
		this.w = w;
	}

	public void setHeight(float h)
	{
		this.h = h;
	}

	public void setAnimation(Animation anim)
	{
		anim.setPlayMode(PlayMode.LOOP);
		this.anim = anim;
	}

	@Override
	public void update(float delta)
	{
	
	}

	@Override
	public void render(SpriteBatch batch)
	{

	}

	@Override
	public void serialize(DataOutput out)
	{
		try
		{
			out.writeFloat(x);
			out.writeFloat(y);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deserialize(DataInput in)
	{

	}

}
