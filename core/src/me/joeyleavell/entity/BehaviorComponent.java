package me.joeyleavell.entity;

public abstract class BehaviorComponent extends Component
{

	public BehaviorComponent(Entity entity)
	{
		super(entity);
	}

	public abstract void update(float delta);

}
