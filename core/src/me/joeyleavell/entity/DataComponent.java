package me.joeyleavell.entity;

import java.io.DataInput;
import java.io.DataOutput;

public abstract class DataComponent extends Component
{

	public DataComponent(Entity entity)
	{
		super(entity);
	}

	public abstract void serialize(DataOutput out);

	public abstract void deserialize(DataInput in);

}
