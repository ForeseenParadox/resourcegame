package me.joeyleavell.entity;

import me.joeyleavell.world.Location;

public class BehaviorVillagerAi extends BehaviorComponent
{

	private DataPosition pos;
	private DataHouseBuilding house;
	private BehaviorPathFollow pathFollow;
	private boolean heading;

	public BehaviorVillagerAi(Entity entity)
	{
		super(entity);

		pos = entity.getDataComponent(DataPosition.class);
		pathFollow = entity.getBehaviorComponent(BehaviorPathFollow.class);
	}

	public void setHomeEntity(Entity ent)
	{
		house = ent.getDataComponent(DataHouseBuilding.class);
	}

	@Override
	public void update(float delta)
	{
		float time = getEntity().getWorld().getTime();
		if (time < 20)
		{
			if (!pathFollow.isMoving() && Math.random() > .995)
			{
				float range = 10;
				float theta = (float) (Math.random() * 2 * Math.PI);
				float sin = (float) Math.sin(theta) * range;
				float cos = (float) Math.cos(theta) * range;

				pathFollow.moveTo(getEntity().getWorld(), pos.getTileX() + (int) cos, pos.getTileY() + (int) sin);
			}
		} else
		{
			// go inside at night

			if (!heading)
			{
				Location houseLoc = house.getHouseLocation();
				pathFollow.moveTo(houseLoc.getWorld(), houseLoc.getX(), houseLoc.getY() - 1);
				heading = true;
			}
			if (!pathFollow.isMoving() && heading)
			{
				house.insertEntity(getEntity());
				heading = false;
			}
		}

	}

}
