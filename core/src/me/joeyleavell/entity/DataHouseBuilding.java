package me.joeyleavell.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.util.ArrayList;
import java.util.List;

import me.joeyleavell.building.Building;
import me.joeyleavell.world.Location;

public class DataHouseBuilding extends DataComponent
{

	private Building house;
	private Location houseLocation;
	private List<Entity> inhabitants;

	public DataHouseBuilding(Entity ent)
	{
		super(ent);
		this.inhabitants = new ArrayList<Entity>();
	}

	public void setHouse(Building house)
	{
		this.house = house;
		this.houseLocation = house.getMultiTile().getLocation();
	}

	public Building getHouse()
	{
		return house;
	}

	public Location getHouseLocation()
	{
		return houseLocation;
	}

	public int getInhabitantsCount()
	{
		return inhabitants.size();
	}

	public void insertEntity(Entity e)
	{
		inhabitants.add(e);
		e.remove();
	}

	public void removeEntity(Entity e)
	{
		inhabitants.remove(e);
		getEntity().getWorld().addEntity(e);
	}

	public void removeAll()
	{
		for (Entity e : inhabitants)
			getEntity().getWorld().addEntity(e);
		inhabitants.clear();
	}

	@Override
	public void serialize(DataOutput out)
	{

	}

	@Override
	public void deserialize(DataInput in)
	{

	}

}
