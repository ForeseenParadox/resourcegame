package me.joeyleavell.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class DataSize extends DataComponent
{

	public int width;
	public int height;

	public DataSize(Entity entity)
	{
		super(entity);
	}

	public DataSize(Entity entity, int width, int height)
	{
		super(entity);
		this.width = width;
		this.height = height;
	}

	@Override
	public void serialize(DataOutput out)
	{
		try
		{
			out.writeInt(width);
			out.writeInt(height);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deserialize(DataInput in)
	{
		try
		{
			width = in.readInt();
			height = in.readInt();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
