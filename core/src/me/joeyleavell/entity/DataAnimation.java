package me.joeyleavell.entity;

import java.io.DataInput;
import java.io.DataOutput;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

public class DataAnimation extends DataComponent
{

	public Animation anim;

	public DataAnimation(Entity entity, Animation anim)
	{
		super(entity);
		this.anim = anim;
		anim.setPlayMode(PlayMode.LOOP);
	}

	@Override
	public void serialize(DataOutput out)
	{

	}

	@Override
	public void deserialize(DataInput in)
	{

	}

}
