package me.joeyleavell.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import me.joeyleavell.GameSerializable;
import me.joeyleavell.world.World;

public class Entity implements GameSerializable
{

	private World world;
	private int id;
	private List<DataComponent> dataComponents;
	private List<BehaviorComponent> behaviorComponents;
	private boolean markedForRemoval;
	private boolean visible;

	public Entity(World world, int id)
	{
		this.world = world;
		this.id = id;
		this.visible = true;

		dataComponents = new ArrayList<DataComponent>();
		behaviorComponents = new ArrayList<BehaviorComponent>();
	}

	public boolean isMarkedForRemoval()
	{
		return markedForRemoval;
	}

	public boolean isVisible()
	{
		return visible;
	}

	public void remove()
	{
		markedForRemoval = true;
	}

	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}

	public void addDataComponent(DataComponent component)
	{
		dataComponents.add(component);
	}

	public void addBehaviorComponent(BehaviorComponent component)
	{
		behaviorComponents.add(component);
	}

	public void removeDataComponent(DataComponent component)
	{
		dataComponents.remove(component);
	}

	public void removeBehaviorComponent(BehaviorComponent component)
	{
		behaviorComponents.remove(component);
	}

	public <T extends DataComponent> T getDataComponent(Class<T> component)
	{
		for (DataComponent c : dataComponents)
			if (c.getClass().isAssignableFrom(component))
				return component.cast(c);
		return null;
	}

	public <T extends BehaviorComponent> T getBehaviorComponent(Class<T> component)
	{
		for (BehaviorComponent c : behaviorComponents)
			if (c.getClass().isAssignableFrom(component))
				return component.cast(c);
		return null;
	}

	public <T extends DataComponent> boolean hasDataComponent(Class<T> component)
	{
		return getDataComponent(component) != null;
	}

	public <T extends BehaviorComponent> boolean hasBehaviorComponent(Class<T> component)
	{
		return getBehaviorComponent(component) != null;
	}

	public World getWorld()
	{
		return world;
	}

	public int getId()
	{
		return id;
	}

	public void update(float delta)
	{
		for (BehaviorComponent behavior : behaviorComponents)
			behavior.update(delta);
	}

	public void render(SpriteBatch batch)
	{
		if (visible)
			for (BehaviorComponent behavior : behaviorComponents)
				if (behavior instanceof Renderable)
					((Renderable) behavior).render(batch);
	}

	public void serialize(DataOutput out)
	{
		for (DataComponent data : dataComponents)
			data.serialize(out);
	}

	public void deserialize(DataInput in)
	{
		for (DataComponent data : dataComponents)
			data.deserialize(in);
	}

}
