package me.joeyleavell.entity;

import com.badlogic.gdx.graphics.g2d.Animation;

import me.joeyleavell.TextureUtil;
import me.joeyleavell.Textures;
import me.joeyleavell.world.World;

public class EntityFactory
{

	public static final int BIRD = 0x00;
	public static final int VILLAGER = 0x01;
	public static final int HOUSE = 0x02;

	public static Entity createBird(World world, float x, float y)
	{
		Entity ent = new Entity(world, BIRD);
		ent.addDataComponent(new DataPosition(ent, x, y));
		ent.addDataComponent(new DataSize(ent, 3, 2));
		ent.addDataComponent(new DataAnimation(ent, new Animation(.5f, TextureUtil.split(Textures.BLUE_BIRD, 3, 2))));
		ent.addBehaviorComponent(new BehaviorAnimate(ent));
		ent.addBehaviorComponent(new BehaviorFlock(ent, y));
		return ent;
	}

	public static Entity createVilager(World world, float x, float y)
	{
		Entity ent = new Entity(world, BIRD);
		ent.addDataComponent(new DataPosition(ent, x, y));
		ent.addDataComponent(new DataSize(ent, 3, 2));
		ent.addDataComponent(new DataAnimation(ent, new Animation(.5f, Textures.VILLAGER_1, Textures.VILLAGER_2, Textures.VILLAGER_3)));
		ent.addBehaviorComponent(new BehaviorAnimate(ent));
		ent.addBehaviorComponent(new BehaviorPathFollow(ent));
		ent.addBehaviorComponent(new BehaviorVillagerAi(ent));
		return ent;
	}

	public static Entity createHouseEntity(World world)
	{
		Entity ent = new Entity(world, HOUSE);
		ent.addDataComponent(new DataHouseBuilding(ent));
		ent.addBehaviorComponent(new BehaviorHouse(ent));
		return ent;
	}

	public static Entity createEntity(World world, int id)
	{
		return createEntity(world, id, 0, 0);
	}

	public static Entity createEntity(World world, int id, float x, float y)
	{
		if (id == BIRD)
			return createBird(world, x, y);
		else if (id == VILLAGER)
			return createVilager(world, x, y);
		else if (id == HOUSE)
			return createHouseEntity(world);
		
		return null;
	}

}
