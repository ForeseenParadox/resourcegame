package me.joeyleavell.entity;

public class BehaviorFlock extends BehaviorComponent
{

	private DataPosition pos;
	private float baseY;
	private float horizontalSpeed;
	private float frequency;
	private float amplitude;

	public BehaviorFlock(Entity entity, float baseY)
	{
		super(entity);

		pos = entity.getDataComponent(DataPosition.class);
		this.baseY = baseY;
		this.horizontalSpeed = .2f;
		this.frequency = .5f;
		this.amplitude = 5f;
	}

	public void setBaseY(float baseY)
	{
		this.baseY = baseY;
	}

	@Override
	public void update(float delta)
	{
		pos.x += horizontalSpeed;
		pos.y = baseY + (float) Math.sin(pos.x * frequency) * amplitude;
	}

}
