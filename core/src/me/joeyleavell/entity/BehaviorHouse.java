package me.joeyleavell.entity;

public class BehaviorHouse extends BehaviorComponent
{

	private DataHouseBuilding house;

	public BehaviorHouse(Entity entity)
	{
		super(entity);

		house = entity.getDataComponent(DataHouseBuilding.class);
	}

	@Override
	public void update(float delta)
	{
		if (getEntity().getWorld().getTime() > 6 && getEntity().getWorld().getTime() < 20)
		{
			System.out.println("test");
			house.removeAll();
		}
	}

}
