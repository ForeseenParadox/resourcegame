package me.joeyleavell.entity;

import com.badlogic.gdx.math.Vector2;

import me.joeyleavell.world.Location;
import me.joeyleavell.world.Node;
import me.joeyleavell.world.World;

public class BehaviorPathFollow extends BehaviorComponent
{

	public static final float SPEED = .2f;

	private DataPosition pos;
	private BehaviorAnimate animateBehavior;

	private int endX;
	private int endY;
	private Node path;
	private Vector2 vel;

	public BehaviorPathFollow(Entity entity)
	{
		super(entity);

		pos = entity.getDataComponent(DataPosition.class);
		animateBehavior = entity.getBehaviorComponent(BehaviorAnimate.class);
		vel = new Vector2();
	}

	public void moveTo(Location loc)
	{
		moveTo(loc.getWorld(), loc.getX(), loc.getY());
	}

	public void moveTo(World world, int tileX, int tileY)
	{
		if (world.isChunkGenerated(new Location(world, tileX, tileY)))
		{
			endX = tileX;
			endY = tileY;
			path = world.findPath(pos.getTileX(), pos.getTileY(), endX, endY);
		}
	}

	public boolean isMoving()
	{
		if (path == null)
			return false;

		int dx = endX * World.TILE_SIZE - (int) pos.x;
		int dy = endY * World.TILE_SIZE - (int) pos.y;
		
		return dx != 0 || dy != 0;
	}

	@Override
	public void update(float delta)
	{

		if (animateBehavior != null)
			animateBehavior.setPaused(!isMoving());

		if (isMoving())
		{
			int tx = path.x * World.TILE_SIZE, ty = path.y * World.TILE_SIZE;

			if (Math.abs(pos.x - tx) <= SPEED && Math.abs(pos.y - ty) <= SPEED)
			{
				path = path.next;
				pos.x = tx;
				pos.y = ty;
			} else
			{

				vel.x = tx - pos.x;
				vel.y = ty - pos.y;
				vel.nor().scl(SPEED);

				pos.x += vel.x;
				pos.y += vel.y;
			}
		}
	}

}
