package me.joeyleavell.entity;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import me.joeyleavell.Textures;
import me.joeyleavell.world.World;

public class Builder extends MobileEntity
{

	public Builder(World world)
	{
		super(world, 0x01);

		setWidth(4);
		setHeight(4);

		TextureRegion[] frames = { Textures.BUILDER_1, Textures.BUILDER_2, Textures.BUILDER_3, Textures.BUILDER_2 };
		setAnimation(new Animation(.25f, frames));
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
	}

}
