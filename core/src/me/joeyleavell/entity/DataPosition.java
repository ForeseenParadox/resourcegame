package me.joeyleavell.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import me.joeyleavell.world.World;

public class DataPosition extends DataComponent
{

	public float x;
	public float y;

	public DataPosition(Entity entity)
	{
		super(entity);
	}

	public DataPosition(Entity entity, float x, float y)
	{
		super(entity);
		this.x = x;
		this.y = y;
	}
	
	public int getTileX()
	{
		return (int) (x / World.TILE_SIZE);
	}
	
	public int getTileY()
	{
		return (int) (y / World.TILE_SIZE);
	}
	
	@Override
	public void serialize(DataOutput out)
	{
		try
		{
			out.writeFloat(x);
			out.writeFloat(y);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deserialize(DataInput in)
	{
		try
		{
			x = in.readFloat();
			y = in.readFloat();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
