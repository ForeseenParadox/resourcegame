package me.joeyleavell.entity;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.joeyleavell.Textures;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.Node;
import me.joeyleavell.world.World;

public class Villager extends MobileEntity
{

	/** The Constant SPEED. */
	public static final float SPEED = .3f;

	/** The end y. */
	public int endX, endY;


	public Villager(World world)
	{
		super(world, 0x00);
		
		setWidth(4);
		setHeight(4);

		TextureRegion[] frames = { Textures.VILLAGER_1, Textures.VILLAGER_2, Textures.VILLAGER_3, Textures.VILLAGER_2 };
		setAnimation(new Animation(.25f, frames));

	}
	//


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * me.joeyleavell.set.world.entity.component.EntityComponent#update(float)
	 */
	@Override
	public void update(float delta)
	{
		super.update(delta);

	
	}

}
