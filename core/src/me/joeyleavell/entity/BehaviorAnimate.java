package me.joeyleavell.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BehaviorAnimate extends BehaviorComponent implements Renderable
{

	private DataPosition pos;
	private DataAnimation animComp;
	private float deltaSum;
	private boolean paused;

	public BehaviorAnimate(Entity entity)
	{
		super(entity);
		pos = entity.getDataComponent(DataPosition.class);
		animComp = entity.getDataComponent(DataAnimation.class);
	}

	public boolean isPaused()
	{
		return paused;
	}

	public void setPaused(boolean paused)
	{
		this.paused = paused;
	}

	@Override
	public void update(float delta)
	{
		deltaSum += delta;
		if (paused)
			deltaSum = 0;
	}

	@Override
	public void render(SpriteBatch batch)
	{
		float light = getEntity().getWorld().getLightLevel(pos.getTileX(), pos.getTileY());
		batch.setColor(light, light, light, 1);
		if (animComp != null)
			batch.draw(animComp.anim.getKeyFrame(deltaSum), pos.x, pos.y);
		batch.setColor(Color.WHITE);
	}

}
