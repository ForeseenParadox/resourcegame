package me.joeyleavell.command;

import me.joeyleavell.states.StatePlay;

public interface CommandListener
{

	public void onCommand(Command cmd, StatePlay screen, String label, String[] args);

}
