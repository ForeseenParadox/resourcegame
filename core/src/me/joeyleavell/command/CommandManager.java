package me.joeyleavell.command;

import java.util.ArrayList;
import java.util.List;

import me.joeyleavell.states.StatePlay;

public class CommandManager
{

	private List<Command> commands;
	private List<CommandListener> listeners;

	public CommandManager()
	{
		commands = new ArrayList<Command>();
		listeners = new ArrayList<CommandListener>();
	}

	public void registerCommand(Command cmd)
	{
		commands.add(cmd);
	}

	public void unregisterCommand(Command cmd)
	{
		commands.remove(cmd);
	}

	public void registerListener(CommandListener listener)
	{
		listeners.add(listener);
	}

	public void unregisterListener(CommandListener listener)
	{
		listeners.remove(listener);
	}

	public Command findCommandByLabel(String label)
	{
		Command cmd = null;
		for (Command x : commands)
			if (x.matches(label))
				cmd = x;
		return cmd;
	}

	private void broadcastCommand(Command cmd, StatePlay screen, String label, String[] args)
	{
		for (CommandListener listener : listeners)
			listener.onCommand(cmd, screen, label, args);
	}

	public void handleCommand(StatePlay state, String raw)
	{
		String[] tokens = raw.split(" ");

		if (tokens.length >= 1)
		{
			String label = tokens[0];
			Command cmd = findCommandByLabel(label);

			if (cmd != null)
			{
				String[] args = new String[tokens.length - 1];
				for (int i = 1; i < tokens.length; i++)
					args[i - 1] = tokens[i];

				broadcastCommand(cmd, state, label, args);
			} else
			{
				state.sendConsoleMessage("Could not find command.");
			}
		} else
		{
			// invalid command
			throw new IllegalArgumentException("Command cannot be empty.");
		}
	}

}
