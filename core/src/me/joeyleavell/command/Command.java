package me.joeyleavell.command;

public class Command
{

	private String name;
	private String description;
	private String usage;
	private String[] aliases;

	public Command(String name, String description, String usage)
	{
		this(name, description, usage, null);
	}

	public Command(String name, String description, String usage, String[] aliases)
	{
		this.name = name;
		this.description = description;
		this.usage = usage;
		this.aliases = aliases;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public String getUsage()
	{
		return usage;
	}

	public String[] getAliases()
	{
		return aliases;
	}

	public boolean matches(String label)
	{
		boolean matches = name.equalsIgnoreCase(label);
		if (aliases != null)
		{
			for (String x : aliases)
				if (x.equalsIgnoreCase(label))
					matches = true;
		}
		return matches;
	}

}
