package me.joeyleavell.states;

import java.io.File;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import me.joeyleavell.Game;
import me.joeyleavell.world.WorldSerializer;

public class StateLoadGame extends GameState
{

	private Stage gui;

	public StateLoadGame()
	{
		gui = new Stage(Game.getInstance().getGuiViewport());
	}

	@Override
	public void show()
	{
		WorldSerializer.SAVES_FOLDER.mkdirs();

		TextButton backButton = new TextButton("Back", Game.getSkin());
		Table root = new Table();
		root.setFillParent(true);

		Table bg = new Table();
		Table saveTable = new Table();
		Table saveNameTable = new Table();
		Table playButtonTable = new Table();
		saveNameTable.defaults().pad(5);
		playButtonTable.defaults().pad(2);
		File savesFolder = WorldSerializer.SAVES_FOLDER;
		File[] saves = savesFolder.listFiles();

		for (final File file : saves)
		{
			if (file.getName().endsWith(".sav"))
			{
				String fileName = file.getName();
				String saveName = fileName.substring(0, fileName.indexOf("."));
				TextButton playButton = new TextButton("Play", Game.getSkin());

				saveNameTable.add(new Label(saveName, Game.getSkin())).left().row();
				playButtonTable.add(playButton).width(60).height(20).row();

				playButton.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event, float x, float y)
					{
						super.clicked(event, x, y);

						// load world and push play state
						Game.getScreenManager().getState(StatePlay.class).loadGame(file);
						Game.getScreenManager().pushState(Game.STATE_PLAY);
					}
				});

			}
		}

		backButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Game.getScreenManager().popState();
			}
		});

		root.center();
		saveTable.defaults().pad(50);

		saveNameTable.left();
		playButtonTable.right();
		saveTable.add(saveNameTable);
		saveTable.add(playButtonTable);

		ScrollPane pane = new ScrollPane(saveTable);
		bg.add(pane);
		bg.setBackground(Game.getSkin().getDrawable("brown-bg"));

		root.defaults().pad(5);
		root.add(bg).height(100).row();
		root.add(backButton).fillX();

		gui.addActor(root);

		root.setBackground(new TextureRegionDrawable(new TextureRegion(Game.getMenuBG())));

		Gdx.input.setInputProcessor(gui);
	}

	@Override
	public void render(float delta)
	{
		gui.act(delta);
		gui.draw();
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void hide()
	{

	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub

	}

}
