package me.joeyleavell.states;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.badlogic.gdx.Gdx;

public class StateManager
{

	private Map<Integer, GameState> states;
	private Stack<GameState> stateStack;

	public StateManager()
	{
		states = new HashMap<Integer, GameState>();
		stateStack = new Stack<GameState>();
	}

	public void registerState(int id, GameState state)
	{
		states.put(id, state);
	}

	public void deregisterState(int id)
	{
		states.remove(id);
	}

	public GameState getCurrentState()
	{
		return stateStack.peek();
	}

	public void pushState(int id)
	{
		if (states.get(id) == null)
			throw new IllegalArgumentException("Screen with ID " + id + " not registered.");
		if (!stateStack.isEmpty())
			getCurrentState().hide();
		stateStack.push(states.get(id));
		getCurrentState().show();
		getCurrentState().resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	public <T extends GameState> T getState(Class<T> stateClass)
	{
		Set<Integer> ids = states.keySet();
		for (int i : ids)
			if (states.get(i).getClass() == stateClass)
				return stateClass.cast(states.get(i));
		return null;
	}

	public void popState()
	{
		// Hide current screen.
		getCurrentState().hide();
		getCurrentState().resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		stateStack.pop();

		// Show current screen.
		if (!stateStack.isEmpty())
			getCurrentState().show();
	}

	public void renderCurrent(float delta)
	{
		getCurrentState().render(delta);
	}

	public void resizeCurrent(int width, int height)
	{
		getCurrentState().resize(width, height);
	}

	public void disposeAll()
	{
		for (int id : states.keySet())
			states.get(id).dispose();
	}

}
