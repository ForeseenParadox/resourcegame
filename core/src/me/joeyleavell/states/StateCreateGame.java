package me.joeyleavell.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import me.joeyleavell.Game;

public class StateCreateGame extends GameState
{

	private Stage gui;
	private TextField nameField;
	private TextField seedField;

	public StateCreateGame()
	{
		gui = new Stage(Game.getInstance().getGuiViewport());

		Table root = new Table();
		root.setFillParent(true);
		root.setBackground(new TextureRegionDrawable(new TextureRegion(Game.getMenuBG())));

		root.center();
		root.pad(300);
		root.defaults().pad(10);

		Table fieldTable = new Table();
		fieldTable.defaults().pad(5);

		seedField = new TextField("", Game.getSkin());
		nameField = new TextField("", Game.getSkin());
		TextButton generateButton = new TextButton("Generate!", Game.getSkin());
		fieldTable.add(new Label("Seed: ", Game.getSkin()));
		fieldTable.add(seedField).width(200).height(30).row();
		fieldTable.add(new Label("Name: ", Game.getSkin()));
		fieldTable.add(nameField).width(200).height(30).row();

		root.add(fieldTable).row();
		root.add(generateButton).fillX().expandX().row();

		gui.addActor(root);

		generateButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				if (!seedField.getText().isEmpty())
				{
					int seed = seedField.getText().hashCode();
					String name = nameField.getText();

					Game.getScreenManager().getState(StatePlay.class).initNewGame(seed, name);
					Game.getScreenManager().pushState(Game.STATE_PLAY);
				}
			}
		});
	}

	@Override
	public void show()
	{
		Gdx.input.setInputProcessor(gui);
	}

	@Override
	public void render(float delta)
	{
		gui.act();
		gui.draw();
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void hide()
	{

	}

	@Override
	public void dispose()
	{

	}

}
