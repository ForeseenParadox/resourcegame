package me.joeyleavell.states.options;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import me.joeyleavell.Game;
import me.joeyleavell.GameSettings;
import me.joeyleavell.states.GameState;

public class StateOptions extends GameState
{

	private GameSettings settings;
	private Stage gui;
	private Table settingsPanel;

	public StateOptions(GameSettings settings)
	{
		this.settings = settings;
		gui = new Stage(Game.getInstance().getGuiViewport());

		TextButton gameButton = new TextButton("Game", Game.getSkin());
		TextButton videoButton = new TextButton("Video", Game.getSkin());
		TextButton controlsButton = new TextButton("Controls", Game.getSkin());
		TextButton audioButton = new TextButton("Audio", Game.getSkin());

		TextButton backButton = new TextButton("Back", Game.getSkin());

		Table root = new Table();
		root.setFillParent(true);
		root.defaults().left();
		root.pad(5);
		root.setBackground(new TextureRegionDrawable(new TextureRegion(Game.getMenuBG())));

		Table tabsPanel = new Table();
		tabsPanel.defaults().expandX().fillX();
		tabsPanel.add(gameButton);
		tabsPanel.add(controlsButton);
		tabsPanel.add(videoButton);
		tabsPanel.add(audioButton);

		settingsPanel = new Table();
		settingsPanel.left();
		settingsPanel.pad(20);
		settingsPanel.setBackground(Game.getSkin().getDrawable("brown-bg"));
		settingsPanel.defaults().left().pad(5);

		initGameGui(settingsPanel);

		root.add(tabsPanel).expandX().fillX().row();
		root.add(settingsPanel).expand().fill().row();
		root.add(backButton);

		gui.addActor(root);

		backButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Game.getScreenManager().popState();
			}
		});

		gameButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				initGameGui(settingsPanel);
			}
		});

		videoButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				initVideoGui(settingsPanel);
			}
		});

		audioButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				initAudioGui(settingsPanel);
			}
		});

		controlsButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				initControlsGui(settingsPanel);
			}
		});
	}

	private void initGameGui(Table root)
	{

	}

	private void initVideoGui(Table root)
	{
		root.clear();

		final CheckBox checkBox = new CheckBox("Fullscreen ", Game.getSkin());
		checkBox.setChecked(settings.isFullscreenEnabled());

		checkBox.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				Game.getInstance().getSettings().setFullscreenEnabled(checkBox.isChecked());
			}
		});

		root.add(checkBox);
	}

	private void initControlsGui(Table root)
	{

	}

	private void initAudioGui(Table root)
	{
		root.clear();

		final Slider masterVolumeSilder = new Slider(0, 1, .01f, false, Game.getSkin());
		final Slider sfxVolumeSilder = new Slider(0, 1, .01f, false, Game.getSkin());
		final Slider musicVolumeSilder = new Slider(0, 1, .01f, false, Game.getSkin());

		masterVolumeSilder.setValue(settings.getMasterVolume());
		musicVolumeSilder.setValue(settings.getMusicVolume());
		sfxVolumeSilder.setValue(settings.getSfxVolume());

		root.add(new Label("Audio", Game.getSkin())).row();
		root.add(new Label("Master Volume", Game.getSkin()));
		root.add(masterVolumeSilder).row();
		root.add(new Label("SFX Volume", Game.getSkin()));
		root.add(sfxVolumeSilder).row();
		root.add(new Label("Music Volume", Game.getSkin()));
		root.add(musicVolumeSilder).row();

		masterVolumeSilder.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Game.getInstance().getSettings().setMasterVolume(masterVolumeSilder.getPercent());
			}
		});

		sfxVolumeSilder.addListener(new ChangeListener()
		{

			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Game.getInstance().getSettings().setSfxVolume(sfxVolumeSilder.getPercent());
			}
		});

		musicVolumeSilder.addListener(new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Game.getInstance().getSettings().setMusicVolume(musicVolumeSilder.getPercent());
			}
		});
	}

	@Override
	public void show()
	{
		Gdx.input.setInputProcessor(gui);
	}

	@Override
	public void render(float delta)
	{
		gui.act(delta);
		gui.draw();
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void hide()
	{

	}

	@Override
	public void dispose()
	{

	}

}