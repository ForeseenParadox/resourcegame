package me.joeyleavell.states;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import me.joeyleavell.Game;
import me.joeyleavell.Plugin;

public class StateMods extends GameState
{

	private Stage gui;
	private Table modsTable;
	private TextButton backButton;

	public StateMods()
	{
		gui = new Stage(Game.getInstance().getGuiViewport());

		Table bg = new Table();
		Table root = new Table();
		root.setFillParent(true);
		root.defaults().pad(5);
		root.pad(10);

		modsTable = new Table();
		modsTable.center();
		ScrollPane pane = new ScrollPane(modsTable, Game.getSkin());
		bg.add(pane).row();
		bg.setBackground(Game.getSkin().getDrawable("brown-bg"));

		root.add(bg).height(200).row();

		backButton = new TextButton("Back", Game.getSkin());
		backButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				Game.getScreenManager().popState();
			}
		});
		root.add(backButton).fillX();
		root.setBackground(new TextureRegionDrawable(new TextureRegion(Game.getMenuBG())));

		gui.addActor(root);
	}

	@Override
	public void show()
	{
		modsTable.clear();

		// load plugin buttons
		List<Plugin> mods = Game.getInstance().getModManager().getMods();
		for (Plugin m : mods)
		{
			Table modTable = new Table();
			modTable.defaults().left();
			final Label modName = new Label(m.getName(), Game.getSkin());
			final TextButton expandInfoButton = new TextButton("\\/", Game.getSkin());
			final Label versionLabel = new Label("Version: " + m.getVersion(), Game.getSkin());
			final Label descriptionLabel = new Label("Description: " + m.getDescription(), Game.getSkin());
			final Label authorLabel = new Label("Author: " + m.getAuthor(), Game.getSkin());
			final Label websiteLabel = new Label("Website: " + m.getWebsite(), Game.getSkin());

			versionLabel.setVisible(false);
			descriptionLabel.setVisible(false);
			authorLabel.setVisible(false);
			websiteLabel.setVisible(false);

			expandInfoButton.addListener(new ClickListener()
			{
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					super.clicked(event, x, y);

					if (versionLabel.isVisible())
					{
						expandInfoButton.setText("\\/");

						versionLabel.setVisible(false);
						descriptionLabel.setVisible(false);
						authorLabel.setVisible(false);
						websiteLabel.setVisible(false);
					} else
					{
						expandInfoButton.setText("/\\");

						versionLabel.setVisible(true);
						descriptionLabel.setVisible(true);
						authorLabel.setVisible(true);
						websiteLabel.setVisible(true);
					}

				}
			});

			modTable.add(modName);
			modTable.add(expandInfoButton).row();
			modTable.add(versionLabel);
			modTable.add(descriptionLabel).row();
			modTable.add(authorLabel);
			modTable.add(websiteLabel).row();

			modsTable.add(modTable).row();
		}

		Gdx.input.setInputProcessor(gui);
	}

	@Override
	public void render(float delta)
	{
		gui.act();
		gui.draw();
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void hide()
	{

	}

	@Override
	public void dispose()
	{

	}

}
