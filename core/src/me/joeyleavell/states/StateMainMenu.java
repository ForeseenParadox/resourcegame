package me.joeyleavell.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import me.joeyleavell.Game;

public class StateMainMenu extends GameState
{

	private Stage gui;
	private TextButton newGameButton;
	private TextButton loadGameButton;
	private TextButton optionsButton;
	private TextButton modsButton;
	private TextButton quitButton;

	public StateMainMenu()
	{

		gui = new Stage(Game.getInstance().getGuiViewport());

		Table root = new Table();
		root.setFillParent(true);
		root.defaults().pad(5);
		root.left();

		newGameButton = new TextButton("New Game", Game.getSkin());
		loadGameButton = new TextButton("Load Game", Game.getSkin());
		optionsButton = new TextButton("Options", Game.getSkin());
		modsButton = new TextButton("Mods", Game.getSkin());
		quitButton = new TextButton("Quit", Game.getSkin());

		newGameButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Game.getScreenManager().pushState(Game.STATE_CREATE_GAME);
			}
		});
		loadGameButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Game.getScreenManager().pushState(Game.STATE_LOAD_GAME);
			}
		});

		optionsButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Game.getScreenManager().pushState(Game.STATE_OPTIONS);
			}
		});

		modsButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Game.getScreenManager().pushState(Game.STATE_PLUGINS);
			}
		});

		quitButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				Gdx.app.exit();
			}
		});

		root.add(newGameButton).width(500).row();
		root.add(loadGameButton).width(500).row();
		root.add(optionsButton).width(500).row();
		root.add(modsButton).width(500).row();
		root.add(quitButton).width(500).row();

		TextureRegionDrawable bg = new TextureRegionDrawable(new TextureRegion(Game.getMenuBG()));
		root.setBackground(bg);

		gui.addActor(root);

		show();
	}

	@Override
	public void show()
	{
		Gdx.input.setInputProcessor(gui);
	}

	@Override
	public void render(float delta)
	{
		gui.act();
		gui.draw();
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void hide()
	{

	}

	@Override
	public void dispose()
	{

	}

}
