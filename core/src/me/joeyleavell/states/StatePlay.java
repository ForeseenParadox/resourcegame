package me.joeyleavell.states;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import me.joeyleavell.Game;
import me.joeyleavell.building.Building;
import me.joeyleavell.building.BuildingDefinition;
import me.joeyleavell.event.Event;
import me.joeyleavell.event.EventBuildingPlaced;
import me.joeyleavell.event.EventHandler;
import me.joeyleavell.event.EventTeleport;
import me.joeyleavell.gui.GameHud;
import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.Minimap;
import me.joeyleavell.world.Waypoint;
import me.joeyleavell.world.World;
import me.joeyleavell.world.WorldSerializer;
import me.joeyleavell.world.effect.Cloud;
import me.joeyleavell.world.effect.Rain;

public class StatePlay extends GameState
{

	private World world;
	private WorldSerializer serializer;
	private SpriteBatch batch;
	private ShapeRenderer shapeRenderer;
	private Viewport worldView;
	private OrthographicCamera worldCam;
	private InputMultiplexer multiplexor;

	// gui
	private GameHud hud;

	private Minimap minimap;

	public static final int MINIMAP_WIDTH = 128;
	public static final int MINIMAP_HEIGHT = 128;

	private int gold;

	private List<Building> buildings;

	// Selection variables
	private Location startMouse;
	private Location endMouse;

	private Map<String, Waypoint> waypoints;

	private Vector2 cameraVel;
	private float camMask;

	private boolean paused;

	private List<Cloud> clouds;
	private Rain rain;
	private Weather weather;

	public enum Weather
	{
		CLEAR, CLOUDY, RAINY
	}

	public StatePlay()
	{
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		worldCam = new OrthographicCamera(Game.VIRTUAL_WIDTH, Game.VIRTUAL_HEIGHT);
		worldView = new StretchViewport(Game.VIRTUAL_WIDTH, Game.VIRTUAL_HEIGHT, worldCam);

		// gui
		multiplexor = new InputMultiplexer();

		Game.getInstance().getEventManager().registerEventHandler(new EventHandler()
		{
			@Override
			public void onEvent(Event ev)
			{
				EventBuildingPlaced buildingPlaceEvent = (EventBuildingPlaced) ev;
				Location clickLoc = getMouseTile();
				BuildingDefinition bDef = Game.getInstance().getGameRegistry().getBuilding(buildingPlaceEvent.getName());
				placeBuilding(clickLoc, bDef);

			}
		}, Event.BUILDING_PLACED);

		cameraVel = new Vector2();
		camMask = .95f;

		worldCam.zoom = .4f;

		// input
		multiplexor.addProcessor(new InputAdapter()
		{
			@Override
			public boolean scrolled(int amount)
			{
				if (!paused && hud.worldHasMovementFocus())
				{
					float speed = .03f;
					worldCam.zoom += amount * speed;
					worldCam.update();

					if (worldCam.zoom < .1f)
						worldCam.zoom = .1f;
					if (worldCam.zoom > .5f)
						worldCam.zoom = .5f;
					return true;
				}
				return false;
			}

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button)
			{
				if (!paused)
				{
					if (button == Buttons.RIGHT)
					{
						// open a context menu or something
					}
				}
				return super.touchUp(screenX, screenY, pointer, button);
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer)
			{
				if (!paused && hud.worldHasMovementFocus())
					if (startMouse != null)
						endMouse = getMouseTile();

				return super.touchDragged(screenX, screenY, pointer);
			}

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button)
			{
				if (!paused && hud.worldHasMovementFocus())
				{
					if (button == Buttons.RIGHT)
					{
						// selection options
						Building buildingUnder = getBuildingUnderMouse();
						if (buildingUnder != null)
						{
							destroyBuilding(buildingUnder);
						}
					}

					clearSelection();
					startMouse = getMouseTile();
					endMouse = getMouseTile();

					// update focus areas
					if (!hud.isInPlacementArea() && !hud.isInConsole())
						hud.clearScrollFocus();
					if (!hud.isInConsole())
						hud.clearKeyboardFocus();
				}
				return super.touchDown(screenX, screenY, pointer, button);
			}

		});

		rain = new Rain();
		clouds = new ArrayList<Cloud>();
		clouds.add(new Cloud(10, 10));

		weather = Weather.RAINY;

		buildings = new ArrayList<Building>();
	}

	public GameHud getHud()
	{
		return hud;
	}

	public void setPaused(boolean paused)
	{
		this.paused = paused;
		hud.setPauseMenuVisible(paused);
	}

	public boolean isPaused()
	{
		return paused;
	}

	public boolean doesWaypointExist(String name)
	{
		return waypoints.containsKey(name);
	}

	public void deleteWaypoint(String waypointName)
	{
		waypoints.remove(waypointName);
	}

	public void createWaypoint(String name, Color c)
	{
		waypoints.put(name, new Waypoint(name, getCameraPos(), new Texture(minimap.getPixels()), c));
	}

	public void editWaypoint(String oldName, String newName, Color c)
	{
		Waypoint old = waypoints.get(oldName);
		waypoints.remove(oldName);
		waypoints.put(newName, new Waypoint(newName, old.getLocation(), old.getMinimapSnap(), c));
	}

	public Collection<Waypoint> getWaypoints()
	{
		return waypoints.values();
	}

	public void placeBuilding(Location loc, BuildingDefinition bDef)
	{
		if (hasEnoughGold(bDef.getCost()))
		{
			if (bDef.canPlace(loc))
			{
				Building b = bDef.makeBuilding(loc);
				buildings.add(b);
				setGold(gold - bDef.getCost());
			}
		}
	}

	private void clearSelection()
	{
		endMouse = null;
		startMouse = null;
	}

	public boolean hasSelection()
	{
		return startMouse != null && endMouse != null;
	}

	private void destroyBuilding(Building b)
	{
		b.destroy();
		buildings.remove(b);
	}

	public void initNewGame(int seed, String worldName)
	{
		world = new World(worldName, seed);
		serializer = new WorldSerializer(world);

		world.setTime(12, 0);

		// init waypoints
		waypoints = new HashMap<String, Waypoint>();

		// init hud
		minimap = new Minimap(world, 128, 128);

		if (hud == null)
			hud = new GameHud(Game.getInstance().getGuiViewport(), multiplexor, minimap, waypoints.values());

		// give player starting resources
		setGold(500);

	}

	public void loadGame(File loc)
	{
		serializer = new WorldSerializer(loc);
		serializer.loadWorld();
		world = serializer.getWorld();
		waypoints = new HashMap<String, Waypoint>();
		minimap = new Minimap(world, 128, 128);

		if (hud == null)
			hud = new GameHud(Game.getInstance().getGuiViewport(), multiplexor, minimap, waypoints.values());

		setGold(500);
	}

	public void saveGame()
	{
		serializer.saveWorld();
	}

	public void setGold(int gold)
	{
		this.gold = gold;
		hud.setGoldLabel(gold);
	}

	public World getWorld()
	{
		return world;
	}

	public void setCameraLocation(Location worldLoc)
	{
		worldCam.position.x = worldLoc.getX() * World.TILE_SIZE;
		worldCam.position.y = worldLoc.getY() * World.TILE_SIZE;

		Game.getInstance().getEventManager().broadcastEvent(new EventTeleport(worldLoc));
	}

	public void sendConsoleMessage(String msg)
	{
		hud.sendConsoleMessage(msg);
	}

	public void spawnBuilder(int x, int y)
	{
	}

	public boolean hasEnoughGold(int target)
	{
		return target <= gold;
	}

	public Location getCameraPos()
	{
		return new Location(world, (int) (worldCam.position.x / World.TILE_SIZE), (int) (worldCam.position.y / World.TILE_SIZE));
	}

	@Override
	public void show()
	{
		Gdx.input.setInputProcessor(multiplexor);
	}

	@Override
	public void render(float delta)
	{
		// update
		if (Gdx.input.isKeyJustPressed(Keys.F3))
		{
			Game.debugEnabled = !Game.debugEnabled;
			hud.setDebugInfoVisible(Game.debugEnabled);
		}

		// update camera

		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE))
		{
			setPaused(!isPaused());
		}

		if (world.getTime() > 6 && world.getTime() < 20)
			Game.getInstance().getAudioManager().playDayMusic();
		else
			Game.getInstance().getAudioManager().playNightMusic();

		Location mouseTile = getMouseTile();

		if (!paused)
		{

			// only move if dev world is focused
			Vector2 accel = new Vector2();
			if (hud.worldHasMovementFocus())
			{
				float camAccel = .5f;
				if (Gdx.input.isKeyPressed(Keys.W))
				{
					accel.y += camAccel;
				}
				if (Gdx.input.isKeyPressed(Keys.S))
				{
					accel.y -= camAccel;
				}
				if (Gdx.input.isKeyPressed(Keys.A))
				{
					accel.x -= camAccel;
				}
				if (Gdx.input.isKeyPressed(Keys.D))
				{
					accel.x += camAccel;
				}
			}

			if (accel.x != 0 && accel.y != 0)
				accel.scl(1 / (float) Math.sqrt(2));

			cameraVel.add(accel);
			float maxVel = 8;
			if (cameraVel.len2() >= maxVel * maxVel)
				cameraVel.nor().scl(maxVel);

			if (accel.x == 0)
				cameraVel.x *= camMask;

			if (accel.y == 0)
				cameraVel.y *= camMask;

			worldCam.position.x += cameraVel.x;
			worldCam.position.y += cameraVel.y;

			if (Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) && Gdx.input.isKeyJustPressed(Keys.D))
				hud.setConsoleVisible(!hud.isConsoleVisible());

			hud.setTimeLabel(world.getTime());

			world.update(Gdx.graphics.getDeltaTime(), worldCam);
			worldCam.update();

			// update debug info
			Runtime runtime = Runtime.getRuntime();
			long totalMem = runtime.totalMemory();
			Location chunk = world.getTileChunkLocation((int) mouseTile.getX(), (int) mouseTile.getY());
			hud.setChunkAndLocInfo(mouseTile.getX(), mouseTile.getY(), chunk.getX(), chunk.getY(), totalMem - runtime.freeMemory(), runtime.freeMemory(), totalMem, Gdx.graphics.getFramesPerSecond(), world.getBiome(mouseTile));

			// update minimap
			minimap.updateMinimap(getCameraPos(), waypoints.values());

			// end updating
		}

		world.render(batch, worldCam);

		if (Math.random() > .99)
		{
			if (weather != Weather.RAINY)
				weather = Weather.CLOUDY;
			else
				weather = Weather.RAINY;
		}

		if (weather == Weather.CLOUDY)
		{
			spawnClouds();
		} else if (weather == Weather.CLEAR)
		{

		} else if (weather == Weather.RAINY)
		{
			spawnClouds();
			if (world.getBiome(getCameraPos()).canRain())
			{
				rain.addDrops(worldCam, 10);
			}
		}

		batch.begin();
		rain.updateAndRender(batch, worldCam, delta);
		batch.end();

		// render clouds
		Gdx.gl20.glEnable(GL20.GL_BLEND);
		Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setProjectionMatrix(worldCam.combined);
		shapeRenderer.setColor(new Color(.7f, .7f, .7f, .5f));
		for (int i = 0; i < clouds.size();)
		{
			Cloud c = clouds.get(i);
			if (c.canRemove(worldCam))
				clouds.remove(i);
			else
			{
				c.renderAndUpdate(shapeRenderer, -.1f);
				i++;
			}
		}
		shapeRenderer.end();

		if (!paused)
		{
			// render building
			if (hud.hasPlacement())
			{
				batch.begin();

				BuildingDefinition bDef = hud.getBuildingDragged();
				int cost = bDef.getCost();
				int width = bDef.getWidth();
				int height = bDef.getHeight();

				if (world.validBuildingPlacement(mouseTile.getX(), mouseTile.getY(), width, height) && hasEnoughGold(cost))
					batch.setColor(Color.GREEN);
				else
					batch.setColor(Color.RED);

				batch.draw(bDef.getTexture(), mouseTile.getX() * World.TILE_SIZE, mouseTile.getY() * World.TILE_SIZE);
				batch.end();
			}

			// render rectangular selection area
			if (hasSelection())
			{
				shapeRenderer.setProjectionMatrix(worldCam.combined);
				shapeRenderer.begin(ShapeType.Line);
				shapeRenderer.setColor(Color.GREEN);
				shapeRenderer.rect(getSelectionX() * World.TILE_SIZE, getSelectionY() * World.TILE_SIZE, getSelectionWidth() * World.TILE_SIZE, getSelectionHeight() * World.TILE_SIZE);
				shapeRenderer.end();
			}
		}

		// render outline around visible chunks
		if (Game.debugEnabled)
		{
			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setProjectionMatrix(worldCam.combined);
			shapeRenderer.setColor(Color.WHITE);
			int chunkX = (int) (worldCam.position.x / World.TILE_SIZE / Chunk.SIZE);
			int chunkY = (int) (worldCam.position.y / World.TILE_SIZE / Chunk.SIZE);
			for (int x = -World.CHUNK_VIEW_DST; x <= World.CHUNK_VIEW_DST; x++)
			{
				for (int y = -World.CHUNK_VIEW_DST; y <= World.CHUNK_VIEW_DST; y++)
				{
					int xx = chunkX + x;
					int yy = chunkY + y;
					shapeRenderer.rect(xx * World.TILE_SIZE * Chunk.SIZE, yy * World.TILE_SIZE * Chunk.SIZE, World.TILE_SIZE * Chunk.SIZE, World.TILE_SIZE * Chunk.SIZE);
				}
			}
			shapeRenderer.end();
		}

		// render outline around building under house
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.GRAY);
		Building building = getBuildingUnderMouse();
		if (building != null)
		{
			Location loc = building.getMultiTile().getLocation();
			shapeRenderer.rect(loc.getX() * World.TILE_SIZE, loc.getY() * World.TILE_SIZE, World.TILE_SIZE * building.getWidth(), World.TILE_SIZE * building.getHeight());
		}
		
		shapeRenderer.end();

		hud.render();
	}

	private void spawnClouds()
	{
		if (Math.random() > .2)
		{
			float side = (float) (Math.signum(Math.random() * 2 - 1));
			float horizontalDisplacement = 0;
			float verticalDisplacement = 0;

			if (Math.random() > .5)
			{
				verticalDisplacement = side * Game.VIRTUAL_HEIGHT / 2 + side * 100;
				horizontalDisplacement = (float) ((Math.random() * 2 - 1) * Game.VIRTUAL_WIDTH * 2);
			} else
			{

				horizontalDisplacement = side * Game.VIRTUAL_WIDTH / 2 + side * 100;
				verticalDisplacement = (float) ((Math.random() * 2 - 1) * Game.VIRTUAL_HEIGHT * 2);
			}

			clouds.add(new Cloud(worldCam.position.x + horizontalDisplacement, worldCam.position.y + verticalDisplacement));
		}
	}

	public Building getBuildingUnderMouse()
	{
		Location mouse = getMouseTile();
		Building result = null;
		for (Building building : buildings)
		{
			if (building.isLocationInBuilding(new Location(world, mouse.getX(), mouse.getY())))
				result = building;
		}
		return result;
	}

	public int getSelectionX()
	{
		return (int) Math.min(startMouse.getX(), endMouse.getX());
	}

	public int getSelectionY()
	{
		return (int) Math.min(startMouse.getY(), endMouse.getY());
	}

	public int getSelectionWidth()
	{
		return (int) Math.abs(endMouse.getX() - startMouse.getX()) + 1;
	}

	public int getSelectionHeight()
	{
		return (int) Math.abs(endMouse.getY() - startMouse.getY()) + 1;
	}

	public Location getMouseTile()
	{
		Vector3 mousePress = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		Vector3 worldCoords = worldCam.unproject(mousePress);
		int x = (int) Math.floor(worldCoords.x / World.TILE_SIZE), y = (int) Math.floor(worldCoords.y / World.TILE_SIZE);
		return new Location(world, x, y);
	}

	public Vector2 getBottomLeftScreenTile()
	{
		Vector3 coords = new Vector3(0, Gdx.graphics.getHeight(), 0);
		Vector3 worldCoords = worldCam.unproject(coords);
		int x = (int) (worldCoords.x / World.TILE_SIZE), y = (int) (worldCoords.y / World.TILE_SIZE);
		return new Vector2(x, y);
	}

	public Vector2 getTopRightScreenTile()
	{
		Vector3 coords = new Vector3(Gdx.graphics.getWidth(), 0, 0);
		Vector3 worldCoords = worldCam.unproject(coords);
		int x = (int) (worldCoords.x / World.TILE_SIZE), y = (int) (worldCoords.y / World.TILE_SIZE);
		return new Vector2(x, y);
	}

	@Override
	public void resize(int width, int height)
	{
		worldView.update(width, height);
	}

	@Override
	public void dispose()
	{

	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void hide()
	{

	}
}
