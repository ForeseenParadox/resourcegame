package me.joeyleavell;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ModManager
{

	private String modFolderLoc;
	private List<Plugin> mods;

	public ModManager(String modFolderLoc)
	{
		this.modFolderLoc = modFolderLoc;
	}

	public String getModFolderLocation()
	{
		return modFolderLoc;
	}

	public List<Plugin> getMods()
	{
		return mods;
	}

	public void loadMods(Game game)
	{
		// load plugins
		mods = new ArrayList<Plugin>();
		File modFolder = new File(modFolderLoc);
		if (!modFolder.exists())
			modFolder.mkdir();
		File[] modJars = modFolder.listFiles();
		URL[] uris = new URL[modJars.length];
		for (int i = 0; i < modJars.length; i++)
		{
			try
			{
				uris[i] = modJars[i].toURI().toURL();
			} catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
		}

		for (URL uri : uris)
		{
			String modDataLocation = "jar:" + uri.toString() + "!/plugin.dat";
			try
			{
				URL dataUri = new URL(modDataLocation);
				JarURLConnection conn = (JarURLConnection) dataUri.openConnection();
				Properties modProperties = new Properties();
				modProperties.load(conn.getInputStream());

				String mainClass = modProperties.getProperty("main");

				URL[] classes = { uri };
				ClassLoader loader = new URLClassLoader(classes);
				Class<?> main = loader.loadClass(mainClass);
				Plugin instance = (Plugin) main.newInstance();
				
				loadMod(instance, game, modProperties);
			} catch (IOException e)
			{
				e.printStackTrace();
			} catch (ClassNotFoundException e)
			{
				e.printStackTrace();
			} catch (InstantiationException e)
			{
				e.printStackTrace();
			} catch (IllegalAccessException e)
			{
				e.printStackTrace();
			} catch (IllegalArgumentException e)
			{
				e.printStackTrace();
			} catch (SecurityException e)
			{
				e.printStackTrace();
			}
		}

	}

	public void loadMod(Plugin mod, Game game, Properties modProperties)
	{
		mod.load(modProperties, game);
		mod.onLoad();
		mods.add(mod);
		System.out.println("Loaded " + mod.getName() + ".");
	}

	public void disposeMods()
	{
		for (Plugin p : mods)
			disposeMod(p);
		mods.clear();
	}

	private void disposeMod(Plugin mod)
	{
		mod.onDispose();
		System.out.println("Disposed " + mod.getName() + ".");
	}

}
