package me.joeyleavell.world;

public class Node
{

	public int x;
	public int y;
	public Node next;

	public Node()
	{
		
	}

	public Node(int xp, int yp)
	{
		x = xp;
		y = yp;
	}

	public Node(int xp, int yp, Node p)
	{
		x = xp;
		y = yp;
		next = p;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		boolean result = false;
		if (obj instanceof Node)
		{
			Node node = (Node) obj;
			if (x == node.x && y == node.y)
				result = true;
		}
		return result;
	}

	@Override
	public String toString()
	{
		return x + " " + y;
	}

}
