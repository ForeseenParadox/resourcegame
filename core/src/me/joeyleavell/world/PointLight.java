package me.joeyleavell.world;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import me.joeyleavell.GameSerializable;

public class PointLight implements GameSerializable
{

	public Vector2 position;
	public Color color;
	public float intensity;
	public float quad;
	public float linear;
	public float constant;

	public PointLight()
	{
		position = new Vector2();
		color = Color.WHITE;
	}

	public PointLight(Vector2 pos, Color color, float i, float quad, float linear, float constant)
	{
		this.position = pos;
		this.color = color.cpy();
		this.intensity = i;
		this.quad = quad;
		this.linear = linear;
		this.constant = constant;
	}

	public void update(float delta)
	{

	}

	@Override
	public void serialize(DataOutput out)
	{
		try
		{
			out.writeFloat(position.x);
			out.writeFloat(position.y);
			out.writeFloat(intensity);
			out.writeFloat(quad);
			out.writeFloat(linear);
			out.writeFloat(constant);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deserialize(DataInput in)
	{
		try
		{
			position.x = in.readFloat();
			position.y = in.readFloat();
			intensity = in.readFloat();
			quad = in.readFloat();
			linear = in.readFloat();
			constant = in.readFloat();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
