package me.joeyleavell.world;

public class Location
{

	private int x;
	private int y;
	private World world;

	public Location(World world)
	{
		this.world = world;
		this.x = 0;
		this.y = 0;
	}

	public Location(World world, int x, int y)
	{
		this.world = world;
		this.x = x;
		this.y = y;
	}

	public World getWorld()
	{
		return world;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public float getPixelX()
	{
		return x * World.TILE_SIZE;
	}

	public float getPixelY()
	{
		return y * World.TILE_SIZE;
	}

	// equals

	@Override
	public String toString()
	{
		return "<" + x + ", " + y + ">";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Location)
		{
			Location l = (Location) obj;
			if (l.x == x && l.y == y && l.world.equals(world))
				return true;
		}
		return false;
	}

}