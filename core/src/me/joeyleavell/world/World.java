package me.joeyleavell.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import me.joeyleavell.Game;
import me.joeyleavell.building.BuildingDefinition;
import me.joeyleavell.entity.Entity;
import me.joeyleavell.event.Event;
import me.joeyleavell.event.EventHandler;
import me.joeyleavell.event.EventTileChange;
import me.joeyleavell.world.biome.Biome;
import me.joeyleavell.world.tile.Tile;
import me.joeyleavell.world.tile.TileState;

public class World
{

	public static final int CHUNK_VIEW_DST = 5;
	public static final int TILE_ANIMATION_DELAY = 400;
	public static final int SECONDS_PER_HOUR = 1;

	public static final String BACKGROUND_LAYER = "Background";
	public static final String FOREGROUND_LAYER = "Foreground";

	public static final int TILE_SIZE = 4;
	private String name;

	private long lastTileUpdate;

	private float time;
	private float ambient;

	private List<PointLight> lights;

	private List<Entity> entities;

	private Map<Location, Chunk> chunks;
	private Map<Location, MultiTile> multiTiles;

	private List<Location> updatableTiles;

	private Random worldRand;
	private long seed;

	public World()
	{
		// Create an empty world- don't assign name, seed, or time to anything
		// relevant.
		init();
	}

	public World(String name)
	{
		this(name, System.currentTimeMillis());
	}

	public World(String name, long seed)
	{
		this.name = name;
		initRand(seed);

		init();
	}

	public void loadChunk(Chunk chunk)
	{
		chunks.put(chunk.getChunkCoord(), chunk);
	}

	public int getLoadedChunkCount()
	{
		return chunks.size();
	}

	private void init()
	{

		time = 0;

		lastTileUpdate = System.currentTimeMillis();
		lights = new ArrayList<PointLight>();
		entities = new ArrayList<Entity>();
		chunks = new HashMap<Location, Chunk>();
		multiTiles = new HashMap<Location, MultiTile>();
		updatableTiles = new ArrayList<Location>();

		// block change listener
		Game.getInstance().getEventManager().registerEventHandler(new EventHandler()
		{
			@Override
			public void onEvent(Event ev)
			{
				EventTileChange tileChange = (EventTileChange) ev;

				boolean updatableBefore = Game.getInstance().getGameRegistry().getTile(tileChange.getPreviousId()).isUpdatable();
				boolean updatableAfter = Game.getInstance().getGameRegistry().getTile(tileChange.getNewId()).isUpdatable();

				if (updatableAfter && !updatableBefore)
					updatableTiles.add(tileChange.getLocation());
				else if (updatableBefore)
					updatableTiles.remove(tileChange.getLocation());
			}
		}, Event.TILE_CHANGE);
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void initRand(long seed)
	{
		this.seed = seed;
		worldRand = new Random(seed);
	}

	public MultiTile getMultiTile(Location l)
	{
		return multiTiles.get(l);
	}

	public Iterator<Location> getMultitileLocations()
	{
		return multiTiles.keySet().iterator();
	}

	public List<PointLight> getLights()
	{
		return lights;
	}

	public Iterator<Location> getChunkLocations()
	{
		return chunks.keySet().iterator();
	}

	public Chunk getChunkFromLocation(Location loc)
	{
		return chunks.get(loc);
	}

	public float getTime()
	{
		return time;
	}

	public String getName()
	{
		return name;
	}

	public long getSeed()
	{
		return seed;
	}

	public void addPointLight(PointLight light)
	{
		lights.add(light);
	}

	public void removePointLight(PointLight light)
	{
		lights.remove(light);
	}

	public void setTime(float time)
	{
		this.time = time;
	}

	public void setTime(int hour, int minute)
	{
		if (hour < 0 || hour >= 24)
			throw new IllegalArgumentException("Hour must be >=0 && < 24.");
		if (minute < 0 || minute >= 60)
			throw new IllegalArgumentException("Minute must be >=0 && < 24.");

		time = hour + minute / 60f;
	}

	public void addEntity(Entity e)
	{
		entities.add(e);
	}

	public List<Entity> getEntities()
	{
		return entities;
	}

	public float getAmbient()
	{
		return ambient;
	}

	public Location getTileChunkLocation(int x, int y)
	{
		return new Location(this, (int) Math.floor((float) x / Chunk.SIZE), (int) Math.floor((float) y / Chunk.SIZE));
	}

	public Biome getBiome(Location loc)
	{
		return getBiome(loc.getX(), loc.getY());
	}

	public Biome getBiome(int tileX, int tileY)
	{
		Location chunkLoc = getTileChunkLocation(tileX, tileY);
		Chunk c = chunks.get(chunkLoc);
		if (c != null)
			return c.getBiomeTileCoords(tileX, tileY);
		return null;
	}

	public TileState getBackgroundTileState(int x, int y)
	{
		Location chunkLoc = getTileChunkLocation(x, y);
		Chunk c = chunks.get(chunkLoc);
		TileState t = null;
		if (c != null)
			t = c.getBackgroundTileStateWorldCoords(x, y);
		return t;
	}

	public TileState getForegroundTileState(int x, int y)
	{
		Location chunkLoc = getTileChunkLocation(x, y);
		Chunk c = chunks.get(chunkLoc);
		TileState t = null;

		if (c != null)
			t = c.getForegroundTileStateWorldCoords(x, y);
		return t;
	}

	public float getLightLevel(int x, int y)
	{
		Location chunkLoc = getTileChunkLocation(x, y);
		Chunk c = chunks.get(chunkLoc);
		float light = 0;
		if (c != null)
			light = c.getLightLevelTileCoords(x, y);
		return light;
	}

	public boolean isChunkGenerated(Location tileCoord)
	{
		Location chunkCoord = getTileChunkLocation(tileCoord.getX(), tileCoord.getY());
		return chunks.containsKey(chunkCoord);
	}

	public boolean isChunkInViewDistance(Camera cam, Chunk chunk)
	{
		int chunkX = (int) (cam.position.x / TILE_SIZE / Chunk.SIZE);
		int chunkY = (int) (cam.position.x / TILE_SIZE / Chunk.SIZE);
		Location loc = chunk.getChunkCoord();
		if (loc.getX() >= chunkX - CHUNK_VIEW_DST && loc.getY() <= chunkX + CHUNK_VIEW_DST && loc.getY() >= chunkY - CHUNK_VIEW_DST && loc.getY() <= chunkY + CHUNK_VIEW_DST)
			return true;
		return false;
	}

	public void update(float delta, Camera cam)
	{
		time += delta / SECONDS_PER_HOUR;

		if (time > 24)
			time = 0;

		// update animated tiles
		if (System.currentTimeMillis() - lastTileUpdate >= TILE_ANIMATION_DELAY)
		{
			Game.getInstance().getGameRegistry().updateAnimatedTiles();
			lastTileUpdate = System.currentTimeMillis();
		}

		// update lights
		for (PointLight p : lights)
			p.update(delta);

		// calculate ambient light
		float hoursAway = Math.abs(time - 12);
		ambient = Math.max(1 - hoursAway / 12f, .2f);

		// update entities
		int entityIndex = 0;
		while (entityIndex < entities.size())
		{
			Entity e = entities.get(entityIndex);
			if (e.isMarkedForRemoval())
				entities.remove(entityIndex);
			else
			{
				e.update(delta);
				entityIndex++;
			}
		}

		// update tiles
		for (Location l : updatableTiles)
			getForegroundTileState(l.getX(), l.getY()).update(l);

		// chunk updating
		int chunkX = (int) (cam.position.x / World.TILE_SIZE / Chunk.SIZE);
		int chunkY = (int) (cam.position.y / World.TILE_SIZE / Chunk.SIZE);
		for (int x = -CHUNK_VIEW_DST; x <= CHUNK_VIEW_DST; x++)
		{
			for (int y = -CHUNK_VIEW_DST; y <= CHUNK_VIEW_DST; y++)
			{
				int xx = chunkX + x;
				int yy = chunkY + y;
				Location chunkLoc = new Location(this, xx, yy);
				Chunk c = chunks.get(chunkLoc);
				if (c == null)
				{
					c = new Chunk(chunkLoc);
					chunks.put(chunkLoc, c);
					Game.getInstance().getGameRegistry().generateChunk(c, worldRand, chunkLoc);
				}

				c.updateChunk(this, cam);
			}
		}

	}

	/**
	 * Finds a path from the starting coordinates to the ending coordinates. The
	 * implementation of this algorithm uses an early-exit breadth first
	 * traversal that offers a worst case O(V) time complexity, where V is the
	 * number of tiles that are connected starting at the specified start
	 * coordinate. The path returned will be a single instance of the
	 * {@link Node} class, implemented as a linked list. The path returned will
	 * be a linear connection of nodes, each node perpendicular to the previous
	 * node. The invoker of this method is expected to follow the linked list of
	 * nodes until the list terminates with a null reference.
	 * 
	 * @param sx
	 *            The starting x coordinate.
	 * @param sy
	 *            The starting y coordinate.
	 * @param ex
	 *            The ending x coordinate.
	 * @param ey
	 *            The ending y coordinate.
	 * @return a linked list of connected nodes.
	 */
	public Node findPath(int sx, int sy, int ex, int ey)
	{

		if (sx == ex && sy == ey)
			return null;

		LinkedList<Node> frontier = new LinkedList<Node>();
		Set<Node> visited = new HashSet<Node>();
		frontier.offer(new Node(ex, ey, null));

		while (!frontier.isEmpty())
		{
			Node p = frontier.poll();

			if (p.x == sx && p.y == sy)
				return p;
			else
			{
				Node[] sides =
				{ new Node(p.x - 1, p.y, p), new Node(p.x + 1, p.y, p), new Node(p.x, p.y - 1, p), new Node(p.x, p.y + 1, p) };
				Node[] corners =
				{ new Node(p.x + 1, p.y + 1, p), new Node(p.x - 1, p.y + 1, p), new Node(p.x + 1, p.y - 1, p), new Node(p.x - 1, p.y - 1, p) };

				for (Node n : sides)
				{
					if (isTileLocationWalkable(n.x, n.y) && !visited.contains(n))
					{
						frontier.offer(n);
						visited.add(n);
					}
				}

				if (collidableNeighborCount(p.x, p.y) == 0)
				{
					for (Node n : corners)
					{
						if (isTileLocationWalkable(n.x, n.y) && !visited.contains(n))
						{
							frontier.offer(n);
							visited.add(n);
						}
					}
				}

			}
		}
		return null;
	}

	public int collidableNeighborCount(int tx, int ty)
	{
		int count = 0;
		for (int i = -1; i <= 1; i++)
		{
			for (int j = -1; j <= 1; j++)
			{
				int ax = tx + i;
				int ay = ty + j;
				if ((i == 0 ^ j == 0) && !isTileLocationWalkable(ax, ay))
				{
					count++;
				}
			}
		}
		return count;
	}

	public boolean isGenerated(int x, int y)
	{
		return chunks.get(getTileChunkLocation(x, y)) != null;
	}

	/**
	 * Tests whether there is a tile located at (x, y) on the collision layer,
	 * where tiles that are meant to be collided with are located.
	 * 
	 * @param x
	 *            the x location of the cell.
	 * @param y
	 *            the y location of the cell.
	 * @return whether the foreground layer has a tile at (x, y).
	 */
	public boolean isTileLocationWalkable(int x, int y)
	{
		if (!isGenerated(x, y))
			return false;

		Tile fg = getForegroundTileState(x, y).getTile();
		Tile bg = getBackgroundTileState(x, y).getTile();
		return fg.getMaterial().isWalkable() && bg.getMaterial().isWalkable();
	}

	public boolean isTileLocationFarmable(int x, int y)
	{
		if (!isGenerated(x, y))
			return false;

		Tile fg = getForegroundTileState(x, y).getTile();
		Tile bg = getBackgroundTileState(x, y).getTile();
		return bg.getMaterial().isFarmable() && fg.getMaterial().isWalkable();
	}

	public boolean lineOfSight(int x1, int y1, int x2, int y2)
	{

		int xMin = x1, xMax = x2, yMin = y1, yMax = y2;

		if (x1 > x2)
		{
			int t = xMin;
			xMin = xMax;
			xMax = t;

			t = yMin;
			yMin = yMax;
			yMax = t;
		}

		if (xMax - xMin > yMax - yMin)
			return losStepX(xMin, yMin, xMax, yMax);
		else
			return losStepY(xMin, yMin, xMax, yMax);
	}

	private boolean losStepX(int xMin, int yMin, int xMax, int yMax)
	{
		float yStep = (float) (yMax - yMin) / (xMax - xMin);
		float y = yMin;
		boolean occluder = false;

		// walk through each point on the line and check if there is an occluder
		// in the way
		for (int x = xMin; x <= xMax; x++, y += yStep)
			if (isTileLocationWalkable(x, (int) y))
				occluder = true;

		return !occluder;
	}

	private boolean losStepY(int xMin, int yMin, int xMax, int yMax)
	{
		float xStep = (float) (xMax - xMin) / (yMax - yMin);
		float x = xMin;
		boolean occluder = false;

		// walk through each point on the line and check if there is an occluder
		// in the way
		for (int y = yMin; y <= yMax; y++, x += xStep)
			if (isTileLocationWalkable((int) x, y))
				occluder = true;

		return !occluder;
	}

	public boolean validFarmPlacement(int x, int y, int w, int h)
	{
		boolean valid = true;
		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				if (!isTileLocationFarmable(x + i, y + j))
					valid = false;
		return valid;
	}

	public boolean validBuildingPlacement(int x, int y, int w, int h)
	{
		boolean valid = true;
		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				if (!isTileLocationWalkable(x + i, y + j))
					valid = false;
		return valid;
	}

	public MultiTile placeMultitile(TileState[] tiles, int x, int y, int w, int h)
	{
		for (int xx = 0; xx < w; xx++)
		{
			for (int yy = 0; yy < h; yy++)
			{
				int index = yy * w + xx;
				getForegroundTileState(x + xx, y + yy).setTile(tiles[index].getTile());
				getForegroundTileState(x + xx, y + yy).setMeta(tiles[index].getMeta());
			}
		}

		Location l = new Location(this, x, y);
		MultiTile m = new MultiTile(l, w, h);
		multiTiles.put(l, m);
		return m;
	}

	public MultiTile placeMultitile(Tile tile, int x, int y, int w, int h)
	{
		for (int xx = 0; xx < w; xx++)
		{
			for (int yy = 0; yy < h; yy++)
			{
				int textureMeta = yy * w + xx;
				getForegroundTileState(x + xx, y + yy).setTile(tile);
				getForegroundTileState(x + xx, y + yy).setMeta(textureMeta);
			}
		}

		Location l = new Location(this, x, y);
		MultiTile m = new MultiTile(l, w, h);
		multiTiles.put(l, m);
		return m;
	}

	public MultiTile placeBuilding(BuildingDefinition bDef, Location loc)
	{
		return placeMultitile(bDef.getBuildingTiles(), loc.getX(), loc.getY(), bDef.getWidth(), bDef.getHeight());
	}

	public void render(SpriteBatch batch, Camera cam)
	{
		batch.setProjectionMatrix(cam.combined);

		// chunk rendering
		int chunkX = (int) (cam.position.x / World.TILE_SIZE / Chunk.SIZE);
		int chunkY = (int) (cam.position.y / World.TILE_SIZE / Chunk.SIZE);
		for (int x = -CHUNK_VIEW_DST; x <= CHUNK_VIEW_DST; x++)
		{
			for (int y = -CHUNK_VIEW_DST; y <= CHUNK_VIEW_DST; y++)
			{
				int xx = chunkX + x;
				int yy = chunkY + y;
				Location chunkLoc = new Location(this, xx, yy);
				Chunk c = chunks.get(chunkLoc);

				if (c != null)
					c.renderChunk(batch, cam);
			}
		}

		// render entities
		batch.begin();
		for (Entity e : entities)
			e.render(batch);
		batch.end();

	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof World)
		{
			World w = (World) obj;
			return w.name.equals(name);
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		return name.hashCode();
	}

}
