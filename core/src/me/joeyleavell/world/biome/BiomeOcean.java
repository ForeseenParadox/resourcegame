package me.joeyleavell.world.biome;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;
import me.joeyleavell.world.tile.Tiles;

public class BiomeOcean extends Biome
{

	public BiomeOcean()
	{
		super(0x04, "biome_ocean");
		setCanRain(true);
	}

	@Override
	public void generateBlock(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord, float heightMap)
	{
		if (heightMap > .36)
			world.getBackgroundTileState(x, y).setTile(Tiles.SAND);
		else
			world.getBackgroundTileState(x, y).setTile(Tiles.WATER);
	}

	@Override
	public void populateChunk(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord)
	{

	}

}
