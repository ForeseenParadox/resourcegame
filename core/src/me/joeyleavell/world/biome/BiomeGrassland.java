package me.joeyleavell.world.biome;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;
import me.joeyleavell.world.tile.Tiles;

public class BiomeGrassland extends Biome
{

	public BiomeGrassland()
	{
		super(0x02, "biome_grassland");
		setCanRain(true);
	}

	@Override
	public void generateBlock(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord, float heightMap)
	{
		world.getBackgroundTileState(x, y).setTile(Tiles.LIGHT_GRASS);
	}

	@Override
	public void populateChunk(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord)
	{
		if (rand.nextDouble() > .98 && world.validBuildingPlacement(x, y, 2, 2))
		{
			if (rand.nextBoolean())
				world.getForegroundTileState(x, y).setTile(Tiles.BOULDER_1);
			else
				world.getForegroundTileState(x, y).setTile(Tiles.BOULDER_2);
		}
	}

}
