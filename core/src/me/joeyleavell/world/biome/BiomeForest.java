package me.joeyleavell.world.biome;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;
import me.joeyleavell.world.tile.Tiles;

public class BiomeForest extends Biome
{

	public BiomeForest()
	{
		super(0x01, "biome_forest");
		setCanRain(true);
	}

	@Override
	public void generateBlock(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord, float heightMap)
	{
		world.getBackgroundTileState(x, y).setTile(Tiles.DARK_GRASS);
	}

	@Override
	public void populateChunk(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord)
	{
		if (rand.nextDouble() > .98 && world.validBuildingPlacement(x, y, 2, 2))
		{
			if (rand.nextBoolean())
				world.placeMultitile(Tiles.TREE_1, x, y, 2, 2);
			else
				world.placeMultitile(Tiles.TREE_2, x, y, 2, 2);
		}
	}
}
