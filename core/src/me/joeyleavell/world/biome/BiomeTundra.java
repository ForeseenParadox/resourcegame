package me.joeyleavell.world.biome;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;
import me.joeyleavell.world.tile.Tiles;

public class BiomeTundra extends Biome
{

	public BiomeTundra()
	{
		super(0x05, "biome_tundra");
	}

	@Override
	public void generateBlock(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord, float heightMap)
	{
		world.getBackgroundTileState(x, y).setTile(Tiles.SNOW);
	}

	@Override
	public void populateChunk(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord)
	{
		// TODO Auto-generated method stub
		
	}


}
