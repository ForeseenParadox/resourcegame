package me.joeyleavell.world.biome;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;

public class BiomeMountain extends Biome
{

	public BiomeMountain()
	{
		super(0x03, "biome_mountain");
	}

	@Override
	public void generateBlock(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord, float heightMap)
	{
	}

	@Override
	public void populateChunk(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord)
	{
		
	}

}
