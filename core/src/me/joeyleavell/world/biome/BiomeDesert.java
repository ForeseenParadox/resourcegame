package me.joeyleavell.world.biome;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;
import me.joeyleavell.world.tile.Tiles;

public class BiomeDesert extends Biome
{

	public BiomeDesert()
	{
		super(0x00, "biome_desert");
	}

	@Override
	public void generateBlock(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord, float heightMap)
	{
		world.getBackgroundTileState(x, y).setTile(Tiles.SAND);
	}

	@Override
	public void populateChunk(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord)
	{
		if (rand.nextDouble() > .98)
		{
			double d = rand.nextDouble();
			if (d < .25 && world.validBuildingPlacement(x, y, 1, 1))
			{
				world.placeMultitile(Tiles.CACTUS_1, x, y, 1, 1);
			} else if (d < .5 && world.validBuildingPlacement(x, y, 1, 2))
			{
				world.placeMultitile(Tiles.CACTUS_2, x, y, 1, 2);
			} else if (d < .75 && world.validBuildingPlacement(x, y, 2, 2))
			{
				world.placeMultitile(Tiles.CACTUS_3, x, y, 2, 2);
			} else if (world.validBuildingPlacement(x, y, 2, 2))
			{
				world.placeMultitile(Tiles.CACTUS_4, x, y, 2, 2);
			}
		}
	}

}
