package me.joeyleavell.world.biome;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;

public abstract class Biome
{

	public static final BiomeMountain MOUNTAIN = new BiomeMountain();
	public static final BiomeForest FOREST = new BiomeForest();
	public static final BiomeDesert DESERT = new BiomeDesert();
	public static final BiomeGrassland GRASSLAND = new BiomeGrassland();
	public static final BiomeOcean OCEAN = new BiomeOcean();
	public static final BiomeTundra TUNDRA = new BiomeTundra();

	private int id;
	private String registryName;
	private boolean canRain;

	public Biome(int id, String registryName)
	{
		this.id = id;
		this.registryName = registryName;
		this.canRain = false;
	}

	public int getId()
	{
		return id;
	}

	public String getRegistryName()
	{
		return registryName;
	}

	public boolean canRain()
	{
		return canRain;
	}

	public void setCanRain(boolean canRain)
	{
		this.canRain = canRain;
	}

	public abstract void generateBlock(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord, float heightMap);

	public abstract void populateChunk(World world, Chunk chunk, int x, int y, Random rand, Location chunkCoord);

}
