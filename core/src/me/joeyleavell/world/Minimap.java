package me.joeyleavell.world;

import java.util.Collection;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;

import me.joeyleavell.world.tile.TileState;
import me.joeyleavell.world.tile.Tiles;

public class Minimap
{

	private World w;
	private Texture minimapTexture;
	private Pixmap pixels;
	private int width;
	private int height;

	public Minimap(World w, int width, int height)
	{
		minimapTexture = new Texture(width, height, Format.RGB888);
		pixels = new Pixmap(width, height, Format.RGB888);
		this.w = w;
		this.width = width;
		this.height = height;
	}

	public World getWorld()
	{
		return w;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Texture getMinimapTexture()
	{
		return minimapTexture;
	}

	public Pixmap getPixels()
	{
		return pixels;
	}

	public void setWorld(World w)
	{
		this.w = w;
	}

	public void updateMinimap(Location camPos, Collection<Waypoint> waypoints)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				int relWorldX = x - width / 2;
				int relWorldY = y - height / 2;
				TileState bg = w.getBackgroundTileState(relWorldX + camPos.getX(), relWorldY + camPos.getY());
				TileState fg = w.getForegroundTileState(relWorldX + camPos.getX(), relWorldY + camPos.getY());
				if (bg != null)
				{
					pixels.drawPixel(x, height - y - 1, bg.getTile().getMapColor());
					if (fg.getTile() != Tiles.AIR)
						pixels.drawPixel(x, height - y - 1, fg.getTile().getMapColor());
				} else
				{
					pixels.drawPixel(x, height - y - 1, 0x000000FF);
				}
			}
		}

		final int RECT_WIDTH = 8;
		final int RECT_HEIGHT = 8;

		for (Waypoint waypoint : waypoints)
		{
			int x = waypoint.getLocation().getX();
			int y = waypoint.getLocation().getY();

			int dx = camPos.getX() - x;
			int dy = camPos.getY() - y;

			int ax = width / 2 - dx;
			int ay = height / 2 + dy;

			if (ax < RECT_WIDTH)
				ax = RECT_WIDTH;
			if (ax > width - RECT_WIDTH)
				ax = width - RECT_WIDTH;
			if (ay < RECT_HEIGHT)
				ay = RECT_HEIGHT;
			if (ay > height-RECT_HEIGHT)
				ay = height-RECT_HEIGHT;

			pixels.setColor(waypoint.getColor());
			pixels.fillRectangle(ax - RECT_WIDTH / 2, ay - RECT_HEIGHT / 2, RECT_WIDTH, RECT_HEIGHT);
		}

		// render pixmap to texture
		minimapTexture.draw(pixels, 0, 0);

	}

}