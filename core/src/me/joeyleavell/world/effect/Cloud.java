package me.joeyleavell.world.effect;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import me.joeyleavell.Game;
import me.joeyleavell.world.World;

public class Cloud
{

	public static final int CLOUD_SIZE = 8 * World.TILE_SIZE;

	private List<Vector2> points;
	private int ticksAlive;

	public Cloud(float x, float y)
	{
		points = new ArrayList<Vector2>();
		generate(x, y);
	}

	private void generate(float x, float y)
	{
		Vector2 initial = new Vector2(x, y);
		Stack<Vector2> depthTree = new Stack<Vector2>();
		depthTree.push(initial);
		points.add(initial);

		while (Math.random() > .1 && !depthTree.isEmpty())
		{
			Vector2 current = depthTree.pop();
			for (int i = -1; i <= 1; i++)
				for (int j = -1; j <= 1; j++)
				{
					if (!(i == 0 && j == 0) && Math.random() > .8)
					{
						Vector2 n = new Vector2(current.x + i * CLOUD_SIZE, current.y + j * CLOUD_SIZE);
						depthTree.push(n);
						points.add(n);
					}
				}
		}
	}

	public boolean canRemove(Camera world)
	{
		if (ticksAlive > 10 * 60)
		{
			boolean visible = false;
			for (Vector2 p : points)
			{
				if (p.x > world.position.x - Game.VIRTUAL_WIDTH / 2 && p.x < world.position.x + Game.VIRTUAL_WIDTH / 2)
					if (p.y > world.position.y - Game.VIRTUAL_HEIGHT / 2 && p.y < world.position.y + Game.VIRTUAL_HEIGHT / 2)
						visible = true;
			}

			return !visible;
		}
		return false;
	}

	public void renderAndUpdate(ShapeRenderer shape, float dx)
	{
		ticksAlive++;
		for (Vector2 point : points)
		{
			point.add(dx, 0);
			shape.rect(point.x - CLOUD_SIZE / 2, point.y - CLOUD_SIZE / 2, CLOUD_SIZE, CLOUD_SIZE);
		}
	}

}
