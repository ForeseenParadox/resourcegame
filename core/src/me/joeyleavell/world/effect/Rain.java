package me.joeyleavell.world.effect;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import me.joeyleavell.Game;

public class Rain
{

	private List<RainDrop> rain;

	public Rain()
	{
		rain = new ArrayList<RainDrop>();
	}

	public void addDrops(Camera pos, int amt)
	{
		for (int i = 0; i < amt; i++)
			rain.add(new RainDrop(pos.position.x + (float) (Math.random() * 2 - 1) * Game.VIRTUAL_WIDTH * 2, pos.position.y + Game.VIRTUAL_HEIGHT + 500));
	}

	public void updateAndRender(SpriteBatch batch, Camera cam, float delta)
	{
		int i = 0;
		batch.setColor(1, 1, 1, .8f);
		while (i < rain.size())
		{
			RainDrop r = rain.get(i);
			if (r.hasHitGround(cam))
			{
				rain.remove(i);
			} else
			{
				r.update(delta);
				r.render(batch);
				i++;
			}
		}
		batch.setColor(Color.WHITE);
	}

}
