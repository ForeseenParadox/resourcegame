package me.joeyleavell.world.effect;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.joeyleavell.Game;
import me.joeyleavell.TextureUtil;
import me.joeyleavell.Textures;

public class RainDrop
{

	private Vector2 pos, vel;
	private Animation ani;
	private TextureRegion head;
	private float animationTimer;

	public RainDrop(float x, float y)
	{
		pos = new Vector2(x, y);
		vel = new Vector2(1f, -10f);
		ani = new Animation(.1f, TextureUtil.split(Textures.RAIN_TRAIL, 2, 4));
		ani.setPlayMode(PlayMode.LOOP);
		head = Textures.RAIN_HEAD;
	}

	public boolean hasHitGround(Camera cam)
	{
		return (pos.y > cam.position.y - Game.VIRTUAL_HEIGHT / 2 && pos.y < cam.position.y + Game.VIRTUAL_HEIGHT / 2) && Math.random() > .98;
	}

	public void update(float delta)
	{
		animationTimer += delta;
		vel.set(1f, -7f);
		pos.add(vel);
		ani.setFrameDuration(.1f);
	}

	public void render(SpriteBatch batch)
	{
		batch.draw(ani.getKeyFrame(animationTimer), pos.x, pos.y + 2);
		batch.draw(head, pos.x, pos.y);
	}

}
