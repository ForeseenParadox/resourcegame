package me.joeyleavell.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.badlogic.gdx.utils.DataInput;

import me.joeyleavell.entity.Entity;

public class WorldSerializer
{

	public static final int FILE_VERSION = 0x00;
	public static final File SAVES_FOLDER = new File("./saves");

	private World world;
	private File loc;
	private GZIPOutputStream output;
	private GZIPInputStream input;

	public WorldSerializer(File loc)
	{
		this.loc = loc;
	}

	public WorldSerializer(World world)
	{
		this.world = world;
		this.loc = new File(SAVES_FOLDER, world.getName() + ".sav");
	}

	public World getWorld()
	{
		return world;
	}

	public File getLocation()
	{
		return loc;
	}

	public void saveWorld()
	{
		try
		{
			SAVES_FOLDER.mkdirs();
//			GZIPOutputStream zipOut = new GZIPOutputStream();
			DataOutputStream out = new DataOutputStream(new FileOutputStream(loc));

			// Write header information
			out.writeByte(FILE_VERSION);
			out.writeUTF(world.getName());
			out.writeFloat(world.getTime());
			out.writeLong(world.getSeed());

			List<PointLight> lights = world.getLights();
			out.writeUTF("Lite");
			System.out.println("Saving " + lights.size() + " lights.");
			out.writeInt(lights.size());
			for (PointLight light : lights)
				light.serialize(out);

			System.out.println("Saving " + world.getLoadedChunkCount() + " chunks.");
			Iterator<Location> chunks = world.getChunkLocations();
			while (chunks.hasNext())
			{
				Chunk chunk = world.getChunkFromLocation(chunks.next());
				out.writeUTF("Chnk");
				chunk.serialize(out);
			}

			List<Entity> entities = world.getEntities();
			out.writeUTF("Enty");
			out.writeInt(entities.size());
			for (Entity e : entities)
			{
				out.writeInt(e.getId());
				e.serialize(out);
			}

//			out.flush();
//			zipOut.close();

			System.out.println("Saved world to " + loc.getPath());

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void loadWorld()
	{
		try
		{
			world = new World();
//			GZIPInputStream zipIn = new GZIPInputStream();
			DataInputStream input = new DataInput(new FileInputStream(loc));

			int version = input.readUnsignedByte();
			System.out.println("Reading world at " + loc.getPath());
			System.out.println("File version: " + version);
			world.setName(input.readUTF());
			world.setTime(input.readFloat());
			world.initRand(input.readLong());
			
			System.out.println(world.getName());

			while (input.available() > 0)
			{
				String header = input.readUTF();

				if (header.equalsIgnoreCase("Lite"))
				{
					int count = input.readInt();
					System.out.println("Loading " + count + " lights.");
					for (int i = 0; i < count; i++)
					{
						PointLight light = new PointLight();
						light.deserialize(input);
						world.addPointLight(light);
						System.out.println("reading light");
					}
				} else if (header.equalsIgnoreCase("Chnk"))
				{
					Chunk chunk = new Chunk(new Location(world, 0, 0));
					chunk.deserialize(input);
					world.loadChunk(chunk);
				}
			}

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
