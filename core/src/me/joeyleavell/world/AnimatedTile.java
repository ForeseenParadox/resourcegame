package me.joeyleavell.world;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import me.joeyleavell.world.tile.Tile;

public class AnimatedTile extends Tile
{

	private int currentFrame;

	public AnimatedTile(int id, Material mat, TextureRegion... textures)
	{
		super(id, mat, textures);
	}

	public TextureRegion getCurrentFrame()
	{
		return getTexture(currentFrame);
	}

	public void nextFrame()
	{
		currentFrame = (currentFrame + 1) % getTextures().length;
	}

}
