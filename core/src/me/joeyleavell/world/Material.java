package me.joeyleavell.world;

public class Material
{

	public static final Material AIR = new Material(true, false);
	public static final Material GROUND = new Material(true, true);
	public static final Material SAND = new Material(true, false);
	public static final Material STONE = new Material(false, false);
	public static final Material SNOW = new Material(true, false);
	public static final Material WHEAT = new Material(true, false);
	public static final Material BUILDING = new Material(false, false);
	public static final Material WATER = new Material(false, false);
	public static final Material FIRE = new Material(false, false);

	private boolean walkable;
	private boolean farmable;

	public Material(boolean walkable, boolean farmable)
	{
		this.walkable = walkable;
		this.farmable = farmable;
	}

	public boolean isWalkable()
	{
		return walkable;
	}

	public boolean isFarmable()
	{
		return farmable;
	}

}
