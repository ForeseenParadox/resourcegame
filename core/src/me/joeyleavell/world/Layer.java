package me.joeyleavell.world;

import me.joeyleavell.world.tile.Block;
import me.joeyleavell.world.tile.TileState;
import me.joeyleavell.world.tile.Tiles;

public class Layer
{

	private Block[] blocks;
	private int width;
	private int height;

	public Layer(World world, int chunkX, int chunkY, int width, int height)
	{
		this.blocks = new Block[width * height];
		this.width = width;
		this.height = height;

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				blocks[y * width + x] = new Block(new Location(world, chunkX * Chunk.SIZE + x, chunkY * Chunk.SIZE + y), new TileState(Tiles.AIR));
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Block getBlock(int x, int y)
	{
		return blocks[y * width + x];
	}

	public Block getBlock(int index)
	{
		return blocks[index];
	}

	public Block getBlock(Location loc)
	{
		return getBlock(loc.getX(), loc.getY());
	}
}
