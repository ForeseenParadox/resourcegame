package me.joeyleavell.world.tile;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import me.joeyleavell.TextureUtil;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.Material;
import me.joeyleavell.world.World;

public class Tile
{

	protected int id;
	private String registryName;
	private int mapColor;
	private TextureRegion[] textures;
	private Material mat;
	private boolean updatable;

	public Tile(int id, Material mat, TextureRegion... t)
	{
		this(id, 0xFFFF00FF, mat, t);
	}

	public Tile(int id, int mapColor, Material mat, TextureRegion... t)
	{
		this.id = id;
		this.mapColor = mapColor;
		this.mat = mat;

		this.textures = new TextureRegion[t.length];

		for (int i = 0; i < textures.length; i++)
			textures[i] = TextureUtil.fixBleeding(t[i]);
	}

	public int getId()
	{
		return id;
	}

	public Tile setUpdatable(boolean updatable)
	{
		this.updatable = updatable;
		return this;
	}

	public Tile setRegistryName(String registryName)
	{
		this.registryName = registryName;
		return this;
	}

	public Tile setMapColor(int color)
	{
		this.mapColor = color;
		return this;
	}

	public int getMapColor()
	{
		return mapColor;
	}

	public TextureRegion getTexture(int texture)
	{
		return textures[texture];
	}

	public String getRegistryName()
	{
		return registryName;
	}

	public TextureRegion[] getTextures()
	{
		return textures;
	}

	public Material getMaterial()
	{
		return mat;
	}

	public boolean isUpdatable()
	{
		return updatable;
	}

	public void update(World world, Location loc, TileState state)
	{

	}

}
