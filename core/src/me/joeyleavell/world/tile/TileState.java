package me.joeyleavell.world.tile;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import me.joeyleavell.world.AnimatedTile;
import me.joeyleavell.world.Location;

public class TileState
{

	private Tile tile;
	private int meta;

	public TileState(Tile tile)
	{
		this(tile, 0);
	}

	public TileState(Tile tile, int meta)
	{
		this.tile = tile;
		this.meta = meta;
	}

	public Tile getTile()
	{
		return tile;
	}

	public int getMeta()
	{
		return meta;
	}

	public TextureRegion getTextureFromMeta()
	{
		if (tile instanceof AnimatedTile)
			return ((AnimatedTile) tile).getCurrentFrame();
		else
			return tile.getTexture(meta);
	}

	public void setTile(Tile tile)
	{
		this.tile = tile;
	}

	public void setMeta(int meta)
	{
		this.meta = meta;
	}

	public void update(Location loc)
	{
		tile.update(loc.getWorld(), loc, this);
	}

}
