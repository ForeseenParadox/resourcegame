package me.joeyleavell.world.tile;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import me.joeyleavell.Game;
import me.joeyleavell.GameSerializable;
import me.joeyleavell.event.EventTileChange;
import me.joeyleavell.world.Location;

public class Block implements GameSerializable
{

	private Location loc;
	private TileState tileState;

	public Block(Location loc, TileState state)
	{
		this.loc = loc;
		this.tileState = state;
	}

	public Location getLocation()
	{
		return loc;
	}

	public TileState getTileState()
	{
		return tileState;
	}

	public void setTileState(TileState t)
	{
		Game.getInstance().getEventManager().broadcastEvent(new EventTileChange(loc, tileState.getTile().getRegistryName(), t.getTile().getRegistryName()));
		this.tileState = t;
	}

	@Override
	public void serialize(DataOutput out)
	{
		try
		{
			out.writeInt(tileState.getTile().getId());
			out.writeInt(tileState.getMeta());
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deserialize(DataInput in)
	{
		try
		{
			tileState.setTile(Game.getInstance().getGameRegistry().getTileById(in.readInt()));
			tileState.setMeta(in.readInt());
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
