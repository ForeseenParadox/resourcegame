package me.joeyleavell.world.tile;

import me.joeyleavell.Textures;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.Material;
import me.joeyleavell.world.World;

public class FarmTile extends Tile
{

	public FarmTile(int id, int stage)
	{
		super(id, Material.WHEAT, Textures.FARM_1, Textures.FARM_2, Textures.FARM_3, Textures.FARM_4);
		setUpdatable(true);
	}

	@Override
	public void update(World world, Location loc, TileState state)
	{
		super.update(world, loc, state);

		World w = loc.getWorld();
		float lightLevel = w.getLightLevel(loc.getX(), loc.getY());
		if (lightLevel > .7)
			if (Math.random() < .001)
				state.setMeta(Math.min(state.getMeta() + 1, 3));
	}

}
