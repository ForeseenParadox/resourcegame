package me.joeyleavell.world.tile;

import me.joeyleavell.TextureUtil;
import me.joeyleavell.Textures;
import me.joeyleavell.world.AnimatedTile;
import me.joeyleavell.world.Material;

public class Tiles
{

	public static final Tile AIR = new Tile(0x00, Material.AIR, Textures.VOID).setRegistryName("void").setMapColor(0x000000FF);
	public static final Tile LIGHT_GRASS = new Tile(0x01, Material.GROUND, Textures.LIGHT_GRASS).setRegistryName("light_grass").setMapColor(0x00FF00FF);
	public static final Tile DARK_GRASS = new Tile(0x02, Material.GROUND, Textures.DARK_GRASS).setRegistryName("dark_grass").setMapColor(0x228B22FF);
	public static final Tile DIRT = new Tile(0x03, Material.GROUND, Textures.DIRT).setRegistryName("dirt");
	public static final Tile SAND = new Tile(0x04, Material.SAND, Textures.SAND).setRegistryName("sand").setMapColor(0xFFFF00FF);
	public static final Tile LIGHT_STONE = new Tile(0x05, Material.STONE, Textures.LIGHT_STONE).setRegistryName("light_stone").setMapColor(0xBBBBBBFF);
	public static final Tile DARK_STONE = new Tile(0x06, Material.STONE, Textures.DARK_STONE).setRegistryName("dark_stone").setMapColor(0x888888FF);
	public static final Tile SNOW = new Tile(0x07, Material.SNOW, Textures.SNOW).setRegistryName("snow").setMapColor(0xFFFFFFFF);
	public static final Tile SMALL_HOUSE = new Tile(0x08, Material.BUILDING, TextureUtil.split(Textures.SMALL_HOUSE)).setRegistryName("small_house");
	public static final Tile BIG_HOUSE = new Tile(0x09, Material.BUILDING, TextureUtil.split(Textures.BIG_HOUSE)).setRegistryName("big_house");
	public static final Tile MAGIC_TOWER = new Tile(0x0A, Material.BUILDING, TextureUtil.split(Textures.MAGIC_TOWER)).setRegistryName("magic_tower");
	public static final Tile BARRACKS = new Tile(0x0B, Material.BUILDING, TextureUtil.split(Textures.BARRACKS)).setRegistryName("barracks");
	public static final Tile WATER = new AnimatedTile(0x0C, Material.WATER, Textures.WATER_1, Textures.WATER_2).setRegistryName("water").setMapColor(0x0000FFFF);
	public static final Tile CAMPFIRE = new AnimatedTile(0x0D, Material.FIRE, Textures.CAMPFIRE_1, Textures.CAMPFIRE_2, Textures.CAMPFIRE_3).setRegistryName("campfire");
	public static final Tile FLAG_POLE_BOTTOM = new Tile(0x0E, Material.BUILDING, Textures.FLAG_POLE_BOTTOM).setRegistryName("flag_bottom");
	public static final Tile FLAG_POLE_MIDDLE = new AnimatedTile(0x0F, Material.BUILDING, Textures.FLAG_MIDDLE_1, Textures.FLAG_MIDDLE_2, Textures.FLAG_MIDDLE_3).setRegistryName("flag_middle");
	public static final Tile FLAG_POLE_TOP = new AnimatedTile(0x10, Material.BUILDING, Textures.FLAG_TOP_1, Textures.FLAG_TOP_2, Textures.FLAG_TOP_3).setRegistryName("flag_top");
	public static final Tile WHEAT = new FarmTile(0x11, 0).setRegistryName("wheat");
	public static final Tile BOULDER_1 = new Tile(0x12, Material.BUILDING, Textures.BOULDER_1).setRegistryName("boulder_1").setMapColor(0x888888FF);
	public static final Tile BOULDER_2 = new Tile(0x13, Material.BUILDING, Textures.BOULDER_2).setRegistryName("boulder_2").setMapColor(0x888888FF);
	public static final Tile TREE_1 = new Tile(0x14, Material.BUILDING, TextureUtil.split(Textures.TREE_1)).setRegistryName("tree_1").setMapColor(0x005500FF);
	public static final Tile TREE_2 = new Tile(0x15, Material.BUILDING, TextureUtil.split(Textures.TREE_2)).setRegistryName("tree_2").setMapColor(0x005500FF);
	public static final Tile SHRUB_1 = new Tile(0x16, Material.BUILDING, TextureUtil.split(Textures.SHRUB_1)).setRegistryName("shrub_1").setMapColor(0x005500FF);
	public static final Tile SHRUB_2 = new Tile(0x17, Material.BUILDING, TextureUtil.split(Textures.SHRUB_2)).setRegistryName("shrub_2").setMapColor(0x005500FF);
	public static final Tile CACTUS_1 = new Tile(0x18, Material.BUILDING, TextureUtil.split(Textures.CACTUS_1)).setRegistryName("cactus_1").setMapColor(0x005500FF);
	public static final Tile CACTUS_2 = new Tile(0x19, Material.BUILDING, TextureUtil.split(Textures.CACTUS_2)).setRegistryName("cactus_2").setMapColor(0x005500FF);
	public static final Tile CACTUS_3 = new Tile(0x1A, Material.BUILDING, TextureUtil.split(Textures.CACTUS_3)).setRegistryName("cactus_3").setMapColor(0x005500FF);
	public static final Tile CACTUS_4 = new Tile(0x1B, Material.BUILDING, TextureUtil.split(Textures.CACTUS_4)).setRegistryName("cactus_4").setMapColor(0x005500FF);

}
