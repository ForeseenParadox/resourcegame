package me.joeyleavell.world;

public class QuadraticAttenuation
{

	private float quadratic;
	private float linear;
	private float constant;

	public QuadraticAttenuation(float quadratic, float linear, float constant)
	{
		this.quadratic = quadratic;
		this.linear = linear;
		this.constant = constant;
	}

	public float getQuadratic()
	{
		return quadratic;
	}

	public float getLinear()
	{
		return linear;
	}

	public float getConstant()
	{
		return constant;
	}

	public void setQuadratic(float quadratic)
	{
		this.quadratic = quadratic;
	}

	public void setLinear(float linear)
	{
		this.linear = linear;
	}

	public void setConstant(float constant)
	{
		this.constant = constant;
	}

	public float sample(float distance)
	{
		return 1.0f / (quadratic * distance * distance + linear * distance + constant);
	}

}
