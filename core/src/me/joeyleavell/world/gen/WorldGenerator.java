package me.joeyleavell.world.gen;

import java.util.Random;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;

public abstract class WorldGenerator
{

	public abstract void generateChunk(Chunk chunk, Random rand, Location chunkCoord);

}
