package me.joeyleavell.world.gen;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.flowpowered.noise.module.Module;
import com.flowpowered.noise.module.source.Perlin;

import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.World;
import me.joeyleavell.world.biome.Biome;
import me.joeyleavell.world.tile.Tiles;

public class DefaultWorldGenerator extends WorldGenerator
{

	public static final float FREQUENCY = .5f;

	private Vector2 terrainOffset;
	private Vector2 temperatureOffset;
	private Vector2 moistureOffset;
	private Perlin terrain;
	private Perlin temperature;
	private Perlin moisture;
	private boolean initialized;
	private Biome[][] BIOMES =
	{
			{ Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA },
			{ Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA, Biome.TUNDRA },
			{ Biome.GRASSLAND, Biome.GRASSLAND, Biome.FOREST, Biome.FOREST, Biome.FOREST, Biome.FOREST },
			{ Biome.DESERT, Biome.DESERT, Biome.FOREST, Biome.FOREST, Biome.FOREST, Biome.FOREST },
			{ Biome.DESERT, Biome.DESERT, Biome.DESERT, Biome.DESERT, Biome.FOREST, Biome.FOREST },
			{ Biome.DESERT, Biome.DESERT, Biome.DESERT, Biome.DESERT, Biome.FOREST, Biome.FOREST } };

	private enum MoistureType
	{
		DRYEST(0x00), DRYER(0x01), DRY(0x02), WET(0x03), WETTER(0x04), WETTEST(0x05);

		private int value;

		private MoistureType(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}
	}

	private enum TemperatureType
	{
		COLDEST(0x00), COLDER(0x01), COLD(0x02), HOT(0x03), HOTTER(0x04), HOTTEST(0x05);

		private int value;

		private TemperatureType(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}
	}

	public DefaultWorldGenerator()
	{
		initialized = false;
	}

	private void init(Random rand)
	{
		terrainOffset = new Vector2((float) (rand.nextDouble() * 2 - 1) * 100000, (float) (rand.nextDouble() * 2 - 1) * 100000);
		temperatureOffset = new Vector2((float) (rand.nextDouble() * 2 - 1) * 100000, (float) (rand.nextDouble() * 2 - 1) * 100000);
		moistureOffset = new Vector2((float) (rand.nextDouble() * 2 - 1) * 100000, (float) (rand.nextDouble() * 2 - 1) * 100000);

		terrain = new Perlin();
		terrain.setFrequency(FREQUENCY);
		temperature = new Perlin();
		temperature.setFrequency(.1f);
		moisture = new Perlin();
		moisture.setFrequency(FREQUENCY);

		initialized = true;
	}

	@Override
	public void generateChunk(Chunk chunk, Random rand, Location chunkLoc)
	{

		World w = chunkLoc.getWorld();

		if (!initialized)
			init(rand);

		double[] terrainNoise = generateNoise(terrain, chunkLoc, terrainOffset);
		double[] temperatureNoise = generateNoise(temperature, chunkLoc, temperatureOffset);
		double[] moistureNoise = generateNoise(moisture, chunkLoc, moistureOffset);

		for (int x = 0; x < Chunk.SIZE; x++)
		{
			for (int y = 0; y < Chunk.SIZE; y++)
			{
				double terrainMap = terrainNoise[y * Chunk.SIZE + x];
				double temperatureMap = temperatureNoise[y * Chunk.SIZE + x];
				double moistureMap = moistureNoise[y * Chunk.SIZE + x];
				Biome biome = null;
				MoistureType moisture = null;
				TemperatureType temp = null;

				// convert into 0-1 range
				terrainMap = (terrainMap + 1) / 2;
				temperatureMap = (temperatureMap + 1) / 2;
				moistureMap = (moistureMap + 1) / 2;

				if (moistureMap < 1 / 6f)
					moisture = MoistureType.DRYEST;
				else if (moistureMap < 2 / 6f)
					moisture = MoistureType.DRYER;
				else if (moistureMap < 3 / 6f)
					moisture = MoistureType.DRY;
				else if (moistureMap < 4 / 6f)
					moisture = MoistureType.WET;
				else if (moistureMap < 5 / 6f)
					moisture = MoistureType.WETTER;
				else
					moisture = MoistureType.WETTEST;

				if (temperatureMap < 1 / 6f)
					temp = TemperatureType.HOTTEST;
				else if (temperatureMap < 2 / 6f)
					temp = TemperatureType.HOTTER;
				else if (temperatureMap < 3 / 6f)
					temp = TemperatureType.HOT;
				else if (temperatureMap < 4 / 6f)
					temp = TemperatureType.COLD;
				else if (temperatureMap < 5 / 6f)
					temp = TemperatureType.COLDER;
				else
					temp = TemperatureType.COLDEST;

				biome = BIOMES[moisture.getValue()][temp.getValue()];

				if (terrainMap < .4 && biome != Biome.TUNDRA)
					biome = Biome.OCEAN;

				chunk.setBiome(x, y, biome);
				int worldX = chunk.getWorldX() + x;
				int worldY = chunk.getWorldY() + y;
				biome.generateBlock(w, chunk, worldX, worldY, rand, chunkLoc, (float) terrainMap);
				biome.populateChunk(w, chunk, worldX, worldY, rand, chunkLoc);

				if (w.getBackgroundTileState(worldX, worldY).getTile() == null)
					w.getBackgroundTileState(worldX, worldY).setTile(Tiles.AIR);
				if (w.getForegroundTileState(worldX, worldY).getTile() == null)
					w.getForegroundTileState(worldX, worldY).setTile(Tiles.AIR);

			}
		}
	}

	public double[] generateNoise(Module module, Location chunkLoc, Vector2 offset)
	{

		float scale = .005f;

		double[] noiseArray = new double[Chunk.SIZE * Chunk.SIZE];

		for (int x = 0; x < Chunk.SIZE; x++)
			for (int y = 0; y < Chunk.SIZE; y++)
				noiseArray[y * Chunk.SIZE + x] = module.getValue((chunkLoc.getX() * Chunk.SIZE + x) * scale + offset.x, (chunkLoc.getY() * Chunk.SIZE + y) * scale + offset.y, 0);

		return noiseArray;
	}

}
