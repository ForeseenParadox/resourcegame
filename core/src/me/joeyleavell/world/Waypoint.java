package me.joeyleavell.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;

public class Waypoint
{

	private String name;
	private Location loc;
	private Texture minimapSnap;
	private Color color;

	public Waypoint(String name, Location loc, Texture minimapSnap, Color color)
	{
		this.name = name;
		this.loc = loc;
		this.minimapSnap = minimapSnap;
		this.color = color;
	}

	public String getName()
	{
		return name;
	}

	public Location getLocation()
	{
		return loc;
	}

	public Texture getMinimapSnap()
	{
		return minimapSnap;
	}

	public Color getColor()
	{
		return color;
	}

	public Texture createColorTexture()
	{
		Pixmap map = new Pixmap(1, 1, Format.RGB888);
		// draw the one pixel of the waypoint color
		map.drawPixel(0, 0, ((int) (color.r * 255) << 24) | ((int) (color.g * 255) << 16) | ((int) (color.b * 255) << 8) | 0xFF);
		return new Texture(map);

	}

	@Override
	public String toString()
	{
		return name + ": " + loc.toString();
	}

}
