package me.joeyleavell.world;

import me.joeyleavell.world.tile.Tiles;

public class MultiTile
{

	private Location loc;
	private int w;
	private int h;

	public MultiTile(Location loc, int w, int h)
	{
		this.loc = loc;
		this.w = w;
		this.h = h;
	}

	public Location getLocation()
	{
		return loc;
	}

	public int getWidth()
	{
		return w;
	}

	public int getHeight()
	{
		return h;
	}

	public boolean isLocationInMultiTile(Location l)
	{
		return l.getX() >= loc.getX() && l.getX() < loc.getX() + w && l.getY() >= loc.getY() && l.getY() < loc.getY() + h;
	}

	public void destroy(Chunk chunk)
	{
		World world = loc.getWorld();
		// iterate through all building tiles and destroy them
		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				world.getForegroundTileState(loc.getX() + i, loc.getY() + j).setTile(Tiles.AIR);
	}

}
