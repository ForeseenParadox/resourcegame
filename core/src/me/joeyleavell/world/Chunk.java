package me.joeyleavell.world;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import me.joeyleavell.Game;
import me.joeyleavell.GameSerializable;
import me.joeyleavell.world.biome.Biome;
import me.joeyleavell.world.tile.TileState;
import me.joeyleavell.world.tile.Tiles;

public class Chunk implements GameSerializable
{
	public static final int SIZE = 16;

	private Location chunkCoord;
	private TileState[] background;
	private TileState[] foreground;
	private Biome[] biomes;
	private float[] lightMap;

	public Chunk(Location chunkCoord)
	{
		this.chunkCoord = chunkCoord;

		background = new TileState[SIZE * SIZE];
		foreground = new TileState[SIZE * SIZE];
		biomes = new Biome[SIZE * SIZE];
		lightMap = new float[SIZE * SIZE];

		for (int i = 0; i < SIZE * SIZE; i++)
		{
			background[i] = new TileState(Tiles.AIR);
			foreground[i] = new TileState(Tiles.AIR);
		}
	}

	public Location getChunkCoord()
	{
		return chunkCoord;
	}

	private int tileXtoLayerX(int tileX)
	{
		return Math.abs(tileX - chunkCoord.getX() * SIZE);
	}

	private int tileYtoLayerY(int tileY)
	{
		return Math.abs(tileY - chunkCoord.getY() * SIZE);
	}

	public int getWorldX()
	{
		return chunkCoord.getX() * SIZE;
	}

	public int getWorldY()
	{
		return chunkCoord.getY() * SIZE;
	}

	public float getLightLevel(int layerX, int layerY)
	{
		return lightMap[layerX + layerY * SIZE];
	}

	public float getLightLevel(Location loc)
	{
		return getLightLevel(loc.getX(), loc.getY());
	}

	public boolean isLocationCollidable(int layerX, int layerY)
	{
		Material fg = getForegroundTileState(layerX, layerY).getTile().getMaterial();
		Material bg = getBackgroundTileState(layerX, layerY).getTile().getMaterial();
		return !fg.isWalkable() || !bg.isWalkable();
	}

	public Biome getBiome(int layerX, int layerY)
	{
		return biomes[layerY * SIZE + layerX];
	}

	public void setBiome(int layerX, int layerY, Biome biome)
	{
		biomes[layerY * SIZE + layerX] = biome;
	}

	public Biome getBiomeTileCoords(int tileX, int tileY)
	{
		return biomes[tileXtoLayerX(tileX) * SIZE + tileYtoLayerY(tileY)];
	}

	public void setBiomeTileCoords(int tileX, int tileY, Biome biome)
	{
		biomes[tileXtoLayerX(tileX) * SIZE + tileYtoLayerY(tileY)] = biome;
	}

	public TileState getBackgroundTileState(int layerX, int layerY)
	{
		return background[layerY * SIZE + layerX];
	}

	public TileState getForegroundTileState(int layerX, int layerY)
	{
		return foreground[layerY * SIZE + layerX];
	}

	public boolean isLocationCollidableTileCoords(int tileX, int tileY)
	{
		return isLocationCollidable(tileXtoLayerX(tileX), tileYtoLayerY(tileY));
	}

	public TileState getBackgroundTileStateWorldCoords(int tileX, int tileY)
	{
		return getBackgroundTileState(tileXtoLayerX(tileX), tileYtoLayerY(tileY));
	}

	public TileState getForegroundTileStateWorldCoords(int tileX, int tileY)
	{
		return getForegroundTileState(tileXtoLayerX(tileX), tileYtoLayerY(tileY));
	}

	public float getLightLevelTileCoords(int tileX, int tileY)
	{
		return lightMap[tileXtoLayerX(tileX) + tileYtoLayerY(tileY) * SIZE];
	}

	public float getLightLevelTileCoords(Location loc)
	{
		return getLightLevelTileCoords(loc.getX(), loc.getY());
	}

	public Location toWorldLocation(int tileX, int tileY)
	{
		return new Location(chunkCoord.getWorld(), chunkCoord.getX() * SIZE + tileX, chunkCoord.getY() * SIZE + tileY);
	}

	public void updateChunk(World world, Camera cam)
	{

		int startX = chunkCoord.getX() * SIZE;
		int startY = chunkCoord.getY() * SIZE;

		// calculate lighting
		for (int x = startX; x < startX + SIZE; x++)
		{
			for (int y = startY; y < startY + SIZE; y++)
			{
				int xIndex = x - startX;
				int yIndex = y - startY;
				int index = yIndex * SIZE + xIndex;
				lightMap[index] = world.getAmbient();

				for (PointLight p : world.getLights())
				{
					float dx = (x - p.position.x);
					float dy = (y - p.position.y);
					// add small bias so there isn't a division by zero
					float dist = (float) Math.sqrt(dx * dx + dy * dy) + 0.0001f;
					float atten = 1.0f / (p.quad * dist * dist + p.linear * dist + p.constant) * p.intensity;

					lightMap[index] += atten;

				}

				lightMap[index] = Math.min(lightMap[index], 1);
				lightMap[index] = Math.max(lightMap[index], 0);
			}
		}

		// update tiles

	}

	public void renderChunk(SpriteBatch batch, Camera cam)
	{
		// render tiles
		batch.begin();

		for (int x = 0; x < SIZE; x++)
		{
			for (int y = 0; y < SIZE; y++)
			{
				int index = y * SIZE + x;
				float light = lightMap[index];
				TileState bgTile = background[index];
				TileState fgTile = foreground[index];

				int tileX = (chunkCoord.getX() * SIZE + x) * World.TILE_SIZE;
				int tileY = (chunkCoord.getY() * SIZE + y) * World.TILE_SIZE;
				batch.setColor(light, light, light, 1);
				if (bgTile.getTile() != Tiles.AIR)
					batch.draw(bgTile.getTextureFromMeta(), tileX, tileY);

				if (fgTile.getTile() != Tiles.AIR)
					batch.draw(fgTile.getTextureFromMeta(), tileX, tileY);
			}
		}
		batch.end();
	}

	@Override
	public void serialize(DataOutput out)
	{
		try
		{
			out.writeInt(chunkCoord.getX());
			out.writeInt(chunkCoord.getY());

			// Write background, foreground, and biome data
			for (int i = 0; i < SIZE * SIZE; i++)
			{
				TileState bg = background[i], fg = foreground[i];
				out.writeInt(bg.getMeta());
				out.writeInt(bg.getTile().getId());
				out.writeInt(fg.getMeta());
				out.writeInt(fg.getTile().getId());
				out.writeInt(biomes[i].getId());
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deserialize(DataInput in)
	{
		try
		{
			World currentWorld = chunkCoord.getWorld();
			chunkCoord = new Location(currentWorld, in.readInt(), in.readInt());

			// Write background, foreground, and biome data
			for (int i = 0; i < SIZE * SIZE; i++)
			{
				TileState bg = background[i], fg = foreground[i];
				bg.setMeta(in.readInt());
				bg.setTile(Game.getInstance().getGameRegistry().getTileById(in.readInt()));
				fg.setMeta(in.readInt());
				fg.setTile(Game.getInstance().getGameRegistry().getTileById(in.readInt()));
				biomes[i] = Game.getInstance().getGameRegistry().getBiomeById(in.readInt());
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
