package me.joeyleavell.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class PulsingPointLight extends PointLight
{

	private float time;

	public PulsingPointLight()
	{
		super();
	}

	public PulsingPointLight(Vector2 pos, Color color, float i, float quad, float linear, float constant)
	{
		super(pos, color, i, quad, linear, constant);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);
		time += delta;
		intensity += (float) Math.sin(time) * .01f;
	}

}
