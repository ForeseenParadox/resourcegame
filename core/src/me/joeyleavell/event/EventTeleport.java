package me.joeyleavell.event;

import me.joeyleavell.world.Location;

public class EventTeleport extends Event
{

	private Location tpLoc;

	public EventTeleport(Location tpLoc)
	{
		super(Event.TELEPORT);
	}

	public Location getTeleportLocation()
	{
		return tpLoc;
	}

}
