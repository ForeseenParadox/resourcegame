package me.joeyleavell.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EventManager
{

	private Map<Integer, List<EventHandler>> eventHandlers;

	public EventManager()
	{
		eventHandlers = new HashMap<Integer, List<EventHandler>>();
	}

	public void broadcastEvent(Event e)
	{
		List<EventHandler> handlers = eventHandlers.get(e.getEventId());
		if (handlers != null)
			for (EventHandler handler : handlers)
				handler.onEvent(e);
	}

	public void registerEventHandler(EventHandler handler, int eventId)
	{
		List<EventHandler> handlers = eventHandlers.get(eventId);
		if (handlers == null)
			handlers = new ArrayList<EventHandler>();
		handlers.add(handler);
		eventHandlers.put(eventId, handlers);
	}

	public void unregisterEventHandler(EventHandler handler)
	{
		Set<Integer> keys = eventHandlers.keySet();

		// remove handler from all events
		for (int i : keys)
			if (eventHandlers.get(i).contains(handler))
				eventHandlers.get(i).remove(handler);
	}

}
