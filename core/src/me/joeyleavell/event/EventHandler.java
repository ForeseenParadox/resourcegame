package me.joeyleavell.event;

public interface EventHandler
{

	public void onEvent(Event ev);

}
