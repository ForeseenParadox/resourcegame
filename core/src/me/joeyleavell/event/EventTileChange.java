package me.joeyleavell.event;

import me.joeyleavell.world.Location;

public class EventTileChange extends Event
{

	private static final String PREVIOUS_ID = "prev_id";
	private static final String NEW_ID = "new_id";
	private static final String LOCATION = "location";

	public EventTileChange(Location loc, String prevReg, String newReg)
	{
		super(Event.TILE_CHANGE);

		addField(PREVIOUS_ID, prevReg);
		addField(NEW_ID, newReg);
		addField(LOCATION, loc);
	}

	public Location getLocation()
	{
		return (Location) getField(LOCATION);
	}

	public String getPreviousId()
	{
		return (String) getField(PREVIOUS_ID);
	}

	public String getNewId()
	{
		return (String) getField(NEW_ID);
	}

}
