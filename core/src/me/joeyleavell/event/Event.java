package me.joeyleavell.event;

import java.util.HashMap;
import java.util.Map;

public class Event
{

	public static final int BUILDING_PLACED = 0x00;
	public static final int TILE_CHANGE = 0x01;
	public static final int TELEPORT = 0x02;

	private int eventId;
	private Map<String, Object> fields;

	public Event(int eventId)
	{
		this.eventId = eventId;
		fields = new HashMap<String, Object>();
	}

	public int getEventId()
	{
		return eventId;
	}

	public void addField(String name, Object obj)
	{
		fields.put(name, obj);
	}

	public Object getField(String name)
	{
		return fields.get(name);
	}

	public Integer getInteger(String name)
	{
		return (Integer) fields.get(name);
	}

	public Float getFloat(String name)
	{
		return (Float) fields.get(name);
	}

	public Double getDouble(String name)
	{
		return (Double) fields.get(name);
	}

	public String getString(String name)
	{
		return (String) fields.get(name);
	}
}