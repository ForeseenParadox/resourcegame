package me.joeyleavell.event;

public class EventBuildingPlaced extends Event
{

	private String name;

	public EventBuildingPlaced(String name)
	{
		super(Event.BUILDING_PLACED);

		this.name = name;
	}

	public String getName()
	{
		return name;
	}

}
