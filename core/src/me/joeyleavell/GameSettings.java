package me.joeyleavell;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.utils.Array;

public class GameSettings
{

	private boolean fullscreenEnabled;
	private float masterVolume;
	private float sfxVolume;
	private float musicVolume;

	public GameSettings()
	{
		fullscreenEnabled = false;

		setMasterVolume(0f);
		setSfxVolume(1);
		setMusicVolume(1);
	}

	public boolean isFullscreenEnabled()
	{
		return fullscreenEnabled;
	}

	public float getMasterVolume()
	{
		return masterVolume;
	}

	public float getMusicVolume()
	{
		return musicVolume;
	}

	public float getSfxVolume()
	{
		return sfxVolume;
	}

	public void setFullscreenEnabled(boolean enabled)
	{
		this.fullscreenEnabled = enabled;
		if (enabled)
			Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
		else
			Gdx.graphics.setWindowedMode(Game.VIRTUAL_WIDTH, Game.VIRTUAL_HEIGHT);
	}

	public void setMasterVolume(float val)
	{
		masterVolume = val;

		// update all music
		setMusicVolume(musicVolume);
	}

	public void setSfxVolume(float val)
	{
		sfxVolume = val;
	}

	public void setMusicVolume(float val)
	{
		musicVolume = val;
		Array<Music> music = new Array<Music>();
		Game.getInstance().getAssetManager().getAll(Music.class, music);

		for (Music m : music)
			m.setVolume(masterVolume * musicVolume);
	}

}
