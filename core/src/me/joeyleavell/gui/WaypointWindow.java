package me.joeyleavell.gui;

import java.util.Collection;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import me.joeyleavell.Game;
import me.joeyleavell.states.StatePlay;
import me.joeyleavell.world.Waypoint;

public class WaypointWindow extends Window
{

	private Table waypointsTable;
	private Table backButton;
	private TextButton newWaypointButton;
	private Table root;

	public WaypointWindow(Skin skin)
	{
		super("Waypoints", skin);

		setVisible(false);

		root = new Table();
		root.setFillParent(true);
		root.right().bottom();
		root.pad(10);
		root.defaults().pad(0, 10, 5, 10);

		waypointsTable = new Table();
		waypointsTable.left().bottom();
		waypointsTable.defaults().left().bottom();
		waypointsTable.defaults().pad(3);

		backButton = new TextButton("Back", getSkin());
		newWaypointButton = new TextButton("Create Waypoint", Game.getSkin());

		ScrollPane pane = new ScrollPane(waypointsTable);
		pane.setFillParent(true);
		root.add(pane).row();
		root.add(backButton).fillX().row();
		root.add(newWaypointButton).expandX().fillX();

		addActor(root);
		setMovable(false);
	}

	public Table getBackButton()
	{
		return backButton;
	}

	public TextButton getNewWaypointButton()
	{
		return newWaypointButton;
	}

	public void updateWaypoints(Collection<Waypoint> waypoints)
	{
		waypointsTable.clear();

		for (final Waypoint w : waypoints)
		{

			Table colorBg = new Table();
			Table imageBg = new Table();
			imageBg.setBackground(new TextureRegionDrawable(new TextureRegion(w.getMinimapSnap())));
			colorBg.setBackground(new TextureRegionDrawable(new TextureRegion(w.createColorTexture())));

			TextButton teleportButton = new TextButton("TP", getSkin());
			TextButton editButton = new TextButton("Edit", getSkin());
			TextButton deleteButton = new TextButton("-", getSkin());

			Table waypointInfoTable = new Table();
			waypointInfoTable.defaults().pad(2).left();

			waypointsTable.add(colorBg).width(30).height(30);
			waypointsTable.add(imageBg).width(30).height(30);
			Label name = new Label(w.getName(), getSkin());
			Label coords = new Label(w.getLocation().toString(), getSkin());
			name.setAlignment(Align.left);
			coords.setAlignment(Align.left);
			waypointInfoTable.add(name).row();
			waypointInfoTable.add(coords);
			waypointsTable.add(waypointInfoTable).left();
			waypointsTable.add(teleportButton).width(30).height(30);
			waypointsTable.add(editButton).width(60).height(30);
			waypointsTable.add(deleteButton).width(30).height(30).row();

			teleportButton.addListener(new ClickListener()
			{
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					super.clicked(event, x, y);

					Game.getScreenManager().getState(StatePlay.class).setCameraLocation(w.getLocation());
				}
			});

			editButton.addListener(new ClickListener()
			{
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					super.clicked(event, x, y);
					Game.getScreenManager().getState(StatePlay.class).getHud().editWaypoint(w);
				}
			});

			deleteButton.addListener(new ClickListener()
			{
				@Override
				public void clicked(InputEvent event, float x, float y)
				{
					super.clicked(event, x, y);
					Game.getScreenManager().getState(StatePlay.class).getHud().deleteWaypoint(w);
				}
			});
		}
	}

}
