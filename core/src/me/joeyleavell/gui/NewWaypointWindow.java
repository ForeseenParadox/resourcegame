package me.joeyleavell.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import me.joeyleavell.Game;
import me.joeyleavell.world.Waypoint;

public class NewWaypointWindow extends Window
{

	private Table root;
	private Waypoint editWaypoint;
	private TextField nameField;
	private TextButton okButton;
	private Slider redSlider;
	private Slider greenSlider;
	private Slider blueSlider;

	public NewWaypointWindow(Skin skin)
	{
		super("New Waypoint", skin);

		setVisible(false);

		setSize(300, 250);
		setPosition((Game.VIRTUAL_WIDTH - getWidth()) / 2, (Game.VIRTUAL_HEIGHT - getHeight()) / 2);

		Table fieldsTable = new Table();
		fieldsTable.pad(30);
		fieldsTable.defaults().pad(5).left();

		root = new Table();
		root.setFillParent(true);
		root.left();
		root.pad(50);
		root.defaults().pad(5);

		nameField = new TextField("", skin);
		redSlider = new Slider(0, 255, .1f, false, skin);
		greenSlider = new Slider(0, 255, .1f, false, skin);
		blueSlider = new Slider(0, 255, .1f, false, skin);

		okButton = new TextButton("Ok", skin);
		nameField.setMaxLength(10);

		fieldsTable.add(new Label("Name: ", skin));
		fieldsTable.add(nameField).width(150).row();

		fieldsTable.add(new Label("Red: ", skin));
		fieldsTable.add(redSlider).width(150).row();

		fieldsTable.add(new Label("Green: ", skin));
		fieldsTable.add(greenSlider).width(150).row();

		fieldsTable.add(new Label("Blue: ", skin));
		fieldsTable.add(blueSlider).width(150).row();

		root.add(fieldsTable).row();
		root.add(okButton).expandX().fillX();

		addActor(root);
	}

	public Color getColorOption()
	{
		return new Color(redSlider.getPercent(), greenSlider.getPercent(), blueSlider.getPercent(), 1);
	}

	public Waypoint getEditWaypoint()
	{
		return editWaypoint;
	}

	public void disableEdit()
	{
		editWaypoint = null;
	}

	public void edit(Waypoint waypoint)
	{
		editWaypoint = waypoint;
		System.out.println(waypoint.getName());

		nameField.setText(waypoint.getName());

		redSlider.setValue(waypoint.getColor().r * 255);
		greenSlider.setValue(waypoint.getColor().g * 255);
		blueSlider.setValue(waypoint.getColor().b * 255);

		setName("Edit Waypoint");
	}

	public boolean isEditing()
	{
		return editWaypoint != null;
	}

	@Override
	public void setVisible(boolean visible)
	{
		super.setVisible(visible);

		setModal(visible);

		if (visible && !isEditing())
		{
			nameField.setText("");

			redSlider.setValue((float) Math.random() * 255);
			greenSlider.setValue((float) Math.random() * 255);
			blueSlider.setValue((float) Math.random() * 255);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		super.draw(batch, parentAlpha);

		ShapeRenderer shape = Game.getInstance().getShapeRenderer();
		batch.setProjectionMatrix(batch.getProjectionMatrix());

		shape.begin(ShapeType.Filled);
		shape.setColor(redSlider.getPercent(), greenSlider.getPercent(), blueSlider.getPercent(), 1);
		shape.rect(getX() + 50, getY() + okButton.getY() + 50, getWidth() - 100, 10);
		shape.setColor(Color.WHITE);
		shape.end();
	}

	public String getWaypointName()
	{
		return nameField.getText();
	}

	public TextButton getOkButton()
	{
		return okButton;
	}

}
