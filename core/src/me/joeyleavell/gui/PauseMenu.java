package me.joeyleavell.gui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import me.joeyleavell.Game;
import me.joeyleavell.states.StatePlay;

public class PauseMenu extends Window
{

	private TextButton resumeButton;
	private TextButton optionsButton;
	private TextButton saveButton;
	private TextButton quitButton;

	public PauseMenu(Skin skin)
	{
		super("Pause", skin);

		setSize(300, 300);
		setPosition((Game.VIRTUAL_WIDTH - getWidth()) / 2, (Game.VIRTUAL_HEIGHT - getHeight()) / 2);

		Table root = new Table();
		root.setFillParent(true);
		root.defaults().pad(5);

		resumeButton = new TextButton("Resume", skin);
		optionsButton = new TextButton("Options", skin);
		saveButton = new TextButton("Save", skin);
		quitButton = new TextButton("Quit", skin);

		root.add(resumeButton).width(200).row();
		root.add(optionsButton).width(200).row();
		root.add(saveButton).width(200).row();
		root.add(quitButton).width(200).row();

		resumeButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				Game.getScreenManager().getState(StatePlay.class).setPaused(false);
			}
		});

		optionsButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				Game.getScreenManager().pushState(Game.STATE_OPTIONS);
			}
		});

		saveButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				// Save world
				Game.getScreenManager().getState(StatePlay.class).saveGame();
			}
		});

		quitButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				Game.getScreenManager().getState(StatePlay.class).setPaused(false);
				setVisible(false);
				Game.getScreenManager().pushState(Game.STATE_MAIN_MENU);
			}
		});

		addActor(root);

		setVisible(false);
		setMovable(false);
		setModal(true);
	}

}
