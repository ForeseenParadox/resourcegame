package me.joeyleavell.gui;

import java.util.Collection;
import java.util.Iterator;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;

import me.joeyleavell.Game;
import me.joeyleavell.building.BuildingDefinition;
import me.joeyleavell.event.EventBuildingPlaced;
import me.joeyleavell.states.StatePlay;
import me.joeyleavell.world.Minimap;
import me.joeyleavell.world.Waypoint;
import me.joeyleavell.world.biome.Biome;

public class GameHud
{

	private Stage gui;

	// Bottom center
	private Table centerPane;
	private Table buildingsTable;

	// Bottom left
	private Table leftPane;
	private Window infoWindow;
	private TextButton expandButton;

	private Label populationLabel;
	private Label goldLabel;

	private Label silverLabel;
	private Label copperLabel;
	private Label ironLabel;
	private Label coalLabel;

	private Label buildersLabel;
	private Label farmersLabel;
	private Label villagersLabel;
	private Label soldiersLabel;
	private Label minersLabel;

	// Bottom right
	private Table rightPane;
	private Label timeLabel;

	// Top right
	private Table topRightPane;
	private MinimapComponent minimap;
	private WaypointWindow waypointWindow;
	private NewWaypointWindow newWaypointWindow;
	private TextButton waypointsButton;

	// Hovering
	private Window console;
	private Label consoleHistory;
	private TextField commandField;

	// Top left
	private Table debugTable;
	private Label xLabel;
	private Label yLabel;
	private Label chunkXLabel;
	private Label chunkYLabel;
	private Label usedMemLabel;
	private Label freeMemLabel;
	private Label totalMemLabel;
	private Label fpsLabel;
	private Label biomeLabel;

	private String placementName;
	private boolean inPlacementArea;
	private boolean inConsole;

	// Context menu
	private Collection<Waypoint> waypointData;

	private PauseMenu pause;

	private AlertWindow alertWindow;

	public GameHud(Viewport viewport, InputMultiplexer multiplexor, Minimap minimap, Collection<Waypoint> waypointData)
	{
		gui = new Stage(viewport);
		multiplexor.addProcessor(gui);
		
		this.waypointData = waypointData;

		initBottomLeftPane();
		initCenterPane();
		initBottomRightPane();
		initTopLeftPane();
		initConsole();
		initTopRightPane(minimap);

		pause = new PauseMenu(Game.getSkin());

		gui.addActor(pause);

		clearPlacement();
	}

	public void setPauseMenuVisible(boolean visible)
	{
		pause.setVisible(visible);
	}

	private void initBottomLeftPane()
	{
		leftPane = new Table();
		leftPane.setFillParent(true);
		leftPane.left().bottom();
		leftPane.defaults().left().pad(5);

		infoWindow = new Window("Info", Game.getSkin());
		infoWindow.setMovable(false);
		infoWindow.setVisible(false);
		Table resourcesPane = new Table();
		resourcesPane.setFillParent(true);
		resourcesPane.defaults().pad(5).left();
		resourcesPane.left();
		resourcesPane.pad(20);

		silverLabel = new Label("Silver: 0", Game.getSkin());
		copperLabel = new Label("Copper: 0", Game.getSkin());
		ironLabel = new Label("Iron: 0", Game.getSkin());
		coalLabel = new Label("Coal: 0", Game.getSkin());

		villagersLabel = new Label("Villagers: 0", Game.getSkin());
		buildersLabel = new Label("Builders: 0", Game.getSkin());
		farmersLabel = new Label("Farmers: 0", Game.getSkin());
		soldiersLabel = new Label("Soliders: 0", Game.getSkin());
		minersLabel = new Label("Miners: 0", Game.getSkin());

		resourcesPane.add(new Label("Population", Game.getSkin())).row();
		resourcesPane.add(villagersLabel).row();
		resourcesPane.add(buildersLabel).row();
		resourcesPane.add(farmersLabel).row();
		resourcesPane.add(soldiersLabel).row();
		resourcesPane.add(minersLabel).row();
		resourcesPane.add(new Label("", Game.getSkin())).row();

		resourcesPane.add(new Label("Resources", Game.getSkin())).row();
		resourcesPane.add(silverLabel).row();
		resourcesPane.add(copperLabel).row();
		resourcesPane.add(ironLabel).row();
		resourcesPane.add(coalLabel).row();

		expandButton = new TextButton("/\\", Game.getSkin());

		goldLabel = new Label("Gold: 0", Game.getSkin());
		populationLabel = new Label("Population: 0", Game.getSkin());

		setPopulationLabel(0);
		setVillagersLabel(0);
		setBuildersLabel(0);
		setFarmersLabel(0);
		setMinersLabel(0);
		setSoldiersLabel(0);

		setGoldLabel(0);
		setSilverLabel(0);
		setCopperLabel(0);
		setIronLabel(0);
		setCoalLabel(0);

		expandButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				infoWindow.setVisible(!infoWindow.isVisible());
				if (infoWindow.isVisible())
					expandButton.setText("\\/");
				else
					expandButton.setText("/\\");
			}
		});

		leftPane.add(infoWindow).width(270).height(350).row();
		leftPane.add(expandButton).width(270).row();
		leftPane.add(goldLabel).row();
		leftPane.add(populationLabel).row();

		ScrollPane windowScroll = new ScrollPane(resourcesPane);
		windowScroll.setFillParent(true);
		infoWindow.addActor(windowScroll);

		gui.addActor(leftPane);
	}

	private void initTopRightPane(Minimap mp)
	{
		topRightPane = new Table();
		topRightPane.setFillParent(true);
		topRightPane.top().right();
		topRightPane.pad(5);
		topRightPane.defaults().pad(3);

		minimap = new MinimapComponent(mp.getMinimapTexture(), 3);
		minimap.setBackground(new TextureRegionDrawable(Game.getSkin().getRegion("minimap-bg")));

		topRightPane.add(minimap).width(128).height(128).row();
		topRightPane.add(waypointsButton).width(128).height(20).row();

		gui.addActor(topRightPane);
	}

	public void setDebugInfoVisible(boolean visible)
	{
		debugTable.setVisible(visible);
	}

	private void initTopLeftPane()
	{
		debugTable = new Table();
		debugTable.setVisible(false);
		debugTable.setFillParent(true);
		debugTable.top().left();
		debugTable.defaults().padTop(5).padLeft(5).left();

		xLabel = new Label("", Game.getSkin());
		yLabel = new Label("", Game.getSkin());
		chunkXLabel = new Label("", Game.getSkin());
		chunkYLabel = new Label("", Game.getSkin());
		usedMemLabel = new Label("", Game.getSkin());
		freeMemLabel = new Label("", Game.getSkin());
		totalMemLabel = new Label("", Game.getSkin());
		fpsLabel = new Label("", Game.getSkin());
		biomeLabel = new Label("", Game.getSkin());

		debugTable.add(xLabel).row();
		debugTable.add(yLabel).row();
		debugTable.add(chunkXLabel).row();
		debugTable.add(chunkYLabel).row();
		debugTable.add(usedMemLabel).row();
		debugTable.add(freeMemLabel).row();
		debugTable.add(fpsLabel).row();
		debugTable.add(totalMemLabel).row();
		debugTable.add(biomeLabel).row();

		gui.addActor(debugTable);
	}

	private void initCenterPane()
	{
		centerPane = new Table();
		centerPane.setFillParent(true);
		centerPane.bottom();

		buildingsTable = new Table();
		buildingsTable.defaults().padRight(10).bottom();

		Iterator<String> buildings = Game.getInstance().getGameRegistry().getBuildingNames();
		while (buildings.hasNext())
		{
			final BuildingDefinition building = Game.getInstance().getGameRegistry().getBuilding(buildings.next());
			final Button buildingButton = new Button(new TextureRegionDrawable(building.getTexture()));
			buildingsTable.add(buildingButton).width(building.getWidth() * 32).height(building.getHeight() * 32);

			buildingButton.addListener(new ClickListener()
			{
				@Override
				public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor)
				{
					super.enter(event, x, y, pointer, fromActor);
					buildingButton.setColor(.6f, .6f, .6f, 1);
				}

				@Override
				public void exit(InputEvent event, float x, float y, int pointer, Actor toActor)
				{
					super.exit(event, x, y, pointer, toActor);
					buildingButton.setColor(Color.WHITE);
				}

				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button)
				{
					super.touchUp(event, x, y, pointer, button);
					if (hasPlacement() && button == Buttons.LEFT && !inPlacementArea)
					{
						Game.getInstance().getEventManager().broadcastEvent(new EventBuildingPlaced(building.getRegistryName()));
						clearPlacement();
					}

					if (inPlacementArea)
						clearPlacement();
				}

				@Override
				public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
				{
					placementName = building.getRegistryName();
					return true;
				}
			});
		}
		buildingsTable.row();

		// put cost elements on GUI
		buildings = Game.getInstance().getGameRegistry().getBuildingNames();
		while (buildings.hasNext())
		{
			BuildingDefinition building = Game.getInstance().getGameRegistry().getBuilding(buildings.next());
			buildingsTable.add(new Label(building.getCost() + "", Game.getSkin())).padTop(5).padBottom(2);
		}

		ScrollPane buildingsScroll = new ScrollPane(buildingsTable, Game.getSkin());

		Table buildingsPane = new Table();
		buildingsPane.setBackground(Game.getSkin().getDrawable("button"));
		buildingsPane.addListener(new ClickListener()
		{
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor)
			{
				super.enter(event, x, y, pointer, fromActor);
				inPlacementArea = true;
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor)
			{
				super.exit(event, x, y, pointer, toActor);
				inPlacementArea = false;
			}
		});

		waypointWindow = new WaypointWindow(Game.getSkin());

		newWaypointWindow = new NewWaypointWindow(Game.getSkin());

		waypointsButton = new TextButton("Waypoints", Game.getSkin());
		waypointsButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				waypointWindow.setVisible(!waypointWindow.isVisible());

				if (newWaypointWindow.isVisible())
					newWaypointWindow.setVisible(false);

				waypointWindow.updateWaypoints(waypointData);

				if (!waypointWindow.isVisible())
					gui.setKeyboardFocus(null);
				else
					gui.setKeyboardFocus(waypointWindow);
			}
		});

		waypointWindow.getBackButton().addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				waypointWindow.setVisible(false);
				newWaypointWindow.setVisible(false);

				gui.setKeyboardFocus(null);
				gui.setScrollFocus(null);
			}
		});

		waypointWindow.getNewWaypointButton().addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				newWaypointWindow.setVisible(true);
			}
		});

		newWaypointWindow.getOkButton().addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);

				// create new waypoint
				if (newWaypointWindow.getWaypointName().isEmpty())
				{
					alert("Waypoint name can not be empty!");
				} else
				{
					if (newWaypointWindow.isEditing())
					{
						Waypoint old = newWaypointWindow.getEditWaypoint();
						Game.getScreenManager().getState(StatePlay.class).editWaypoint(old.getName(), newWaypointWindow.getWaypointName(), newWaypointWindow.getColorOption());

						newWaypointWindow.disableEdit();
					} else
					{
						if (Game.getScreenManager().getState(StatePlay.class).doesWaypointExist(newWaypointWindow.getWaypointName()))
							alert("Waypoint already exists with that name!");
						else
							Game.getScreenManager().getState(StatePlay.class).createWaypoint(newWaypointWindow.getWaypointName(), newWaypointWindow.getColorOption());
					}
				}

				newWaypointWindow.setVisible(false);
				waypointWindow.updateWaypoints(Game.getScreenManager().getState(StatePlay.class).getWaypoints());
			}
		});

		buildingsPane.add(buildingsScroll);
		centerPane.defaults().pad(5);
		centerPane.add(waypointWindow).width(400).height(300).row();
		centerPane.add(buildingsPane).width(400);

		buildingsScroll.setFlickScroll(false);
		buildingsScroll.setFadeScrollBars(false);
		buildingsScroll.setForceScroll(true, false);

		gui.addActor(centerPane);
		gui.addActor(newWaypointWindow);
	}

	public void alert(String msg)
	{
		alertWindow = new AlertWindow(Game.getSkin(), msg);
		gui.addActor(alertWindow);
	}

	public boolean hasAlert()
	{
		return alertWindow != null;
	}

	public void removeAlert()
	{
		alertWindow.remove();
		alertWindow = null;
	}

	public void editWaypoint(Waypoint w)
	{
		newWaypointWindow.edit(w);
		newWaypointWindow.setVisible(true);
	}

	public void deleteWaypoint(Waypoint w)
	{
		Game.getScreenManager().getState(StatePlay.class).deleteWaypoint(w.getName());
		waypointWindow.updateWaypoints(Game.getScreenManager().getState(StatePlay.class).getWaypoints());
	}

	private void initBottomRightPane()
	{
		rightPane = new Table();
		rightPane.setFillParent(true);
		rightPane.right().bottom();
		timeLabel = new Label("00:00 AM", Game.getSkin());
		rightPane.add(timeLabel).row();

		gui.addActor(rightPane);
	}

	private void initConsole()
	{
		console = new Window("DevWindow", Game.getSkin());
		consoleHistory = new Label("", Game.getSkin());
		consoleHistory.setAlignment(Align.topLeft);
		final ScrollPane historyScroll = new ScrollPane(consoleHistory, Game.getSkin());
		consoleHistory.setWrap(true);
		commandField = new TextField("", Game.getSkin());
		commandField.setTextFieldListener(new TextFieldListener()
		{
			@Override
			public void keyTyped(TextField textField, char c)
			{
				if (c == '\r' || c == '\n')
				{
					String text = commandField.getText();
					commandField.setText(null);
					if (text.startsWith("/"))
					{
						Game.getInstance().getCommandManager().handleCommand(Game.getScreenManager().getState(StatePlay.class), text.substring(1));
						historyScroll.setScrollPercentY(1);
					}
				}
			}
		});
		Table root = new Table();
		root.setFillParent(true);
		root.defaults().pad(5);
		root.bottom();
		root.add(historyScroll).width(220).row();
		root.add(commandField).width(220).height(25).center().row();
		console.addActor(root);
		console.setSize(270, 300);
		console.addListener(new ClickListener()
		{
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor)
			{
				super.exit(event, x, y, pointer, toActor);
				inConsole = false;
			}

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor)
			{
				super.enter(event, x, y, pointer, fromActor);

				inConsole = true;
			}
		});

		gui.addActor(console);
	}

	public boolean hasPlacement()
	{
		return placementName != null;
	}

	public void clearPlacement()
	{
		placementName = null;
	}

	public void setTimeLabel(float time)
	{
		int hour = (int) time;
		int minute = (int) (60 * (time - hour));

		if (hour >= 12)
		{
			hour = hour - 12;
			if (hour == 0)
				hour = 12;

			timeLabel.setText(String.format("%02d:%02d PM", hour, minute));
		} else
		{
			if (hour == 0)
				hour = 12;
			timeLabel.setText(String.format("%02d:%02d AM", hour, minute));
		}
	}

	public Actor getKeyboardFocus()
	{
		return gui.getKeyboardFocus();
	}

	public Actor getScrollFocus()
	{
		return gui.getScrollFocus();
	}

	public boolean worldHasMovementFocus()
	{
		if (gui.getKeyboardFocus() == null)
			return true;
		if(hasPlacement())
			return false;
		if (gui.getKeyboardFocus().isDescendantOf(centerPane))
			return false;
		if (gui.getKeyboardFocus().isDescendantOf(rightPane))
			return false;
		if (gui.getKeyboardFocus().isDescendantOf(newWaypointWindow))
			return false;

		return true;
	}

	public boolean worldHasScrollFocus()
	{
		if (gui.getScrollFocus() == null)
			return true;

		if (gui.getScrollFocus().isDescendantOf(centerPane))
			return false;

		return true;
	}

	public void sendConsoleMessage(String msg)
	{
		consoleHistory.setText(consoleHistory.getText() + msg + "\n");
	}

	public void clearScrollFocus()
	{
		gui.setScrollFocus(null);
	}

	public void clearKeyboardFocus()
	{
		gui.setKeyboardFocus(null);
	}

	public boolean isInPlacementArea()
	{
		return inPlacementArea;
	}

	public boolean isInConsole()
	{
		return inConsole;
	}

	public void setGoldLabel(int num)
	{
		goldLabel.setText(String.format("Gold    %04d", num));
	}

	public void setSilverLabel(int num)
	{
		silverLabel.setText(String.format("Silver    %04d", num));
	}

	public void setCopperLabel(int num)
	{
		copperLabel.setText(String.format("Copper    %04d", num));
	}

	public void setIronLabel(int num)
	{
		ironLabel.setText(String.format("Iron      %04d", num));
	}

	public void setCoalLabel(int num)
	{
		coalLabel.setText(String.format("Coal      %04d", num));
	}

	public void setVillagersLabel(int num)
	{
		villagersLabel.setText(String.format("Villagers %04d", num));
	}

	public void setBuildersLabel(int num)
	{
		buildersLabel.setText(String.format("Builders  %04d", num));
	}

	public void setMinersLabel(int num)
	{
		minersLabel.setText(String.format("Miners    %04d", num));
	}

	public void setFarmersLabel(int num)
	{
		farmersLabel.setText(String.format("Farmers   %04d", num));
	}

	public void setSoldiersLabel(int num)
	{
		soldiersLabel.setText(String.format("Soldiers  %04d", num));
	}

	public void setPopulationLabel(int num)
	{
		populationLabel.setText(String.format("Population  %04d", num));
	}

	public void setChunkAndLocInfo(float x, float y, int chunkX, int chunkY, long usedMem, long freeMem, long totalMem, int fps, Biome biome)
	{
		xLabel.setText(String.format("X: %.2f", x));
		yLabel.setText(String.format("Y: %.2f", y));
		chunkXLabel.setText(String.format("Chunk X: %d", chunkX));
		chunkYLabel.setText(String.format("Chunk Y: %d", chunkY));
		usedMemLabel.setText(String.format("Used memory: %.2f MB", (usedMem) / (1024f * 1024f)));
		freeMemLabel.setText(String.format("Free memory: %.2f MB", (freeMem) / (1024f * 1024f)));
		totalMemLabel.setText(String.format("Total memory: %.2f MB", (totalMem) / (1024f * 1024f)));
		fpsLabel.setText("FPS: " + fps);
		if (biome != null)
			biomeLabel.setText("Biome: " + biome);
	}

	public BuildingDefinition getBuildingDragged()
	{
		return Game.getInstance().getGameRegistry().getBuilding(placementName);
	}

	public boolean isConsoleVisible()
	{
		return console.isVisible();
	}

	public void setConsoleVisible(boolean visible)
	{
		console.setVisible(visible);
	}

	public void render()
	{
		gui.act();
		gui.draw();
	}

}