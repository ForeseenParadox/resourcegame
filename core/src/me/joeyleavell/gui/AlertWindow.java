package me.joeyleavell.gui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import me.joeyleavell.Game;
import me.joeyleavell.states.StatePlay;

public class AlertWindow extends Window
{

	public AlertWindow(Skin skin, String message)
	{

		super("Alert", skin);

		setSize(300, 150);
		setPosition((Game.VIRTUAL_WIDTH - getWidth()) / 2, (Game.VIRTUAL_HEIGHT - getHeight()) / 2);
		setMovable(false);

		Table root = new Table();
		root.setFillParent(true);
		root.defaults().pad(5);

		TextButton okButton = new TextButton("Ok", skin);
		Label alertLabel = new Label(message, skin);
		alertLabel.setWrap(true);
		alertLabel.setAlignment(Align.center);
		root.add(alertLabel).width(280).row();
		root.add(okButton).expandX().fillX();
		root.pad(50);

		addActor(root);

		setModal(true);

		okButton.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				super.clicked(event, x, y);
				Game.getScreenManager().getState(StatePlay.class).getHud().removeAlert();
			}
		});
	}

}
