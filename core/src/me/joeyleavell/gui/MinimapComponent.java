package me.joeyleavell.gui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class MinimapComponent extends Table
{

	private Texture minimap;
	private int borderSize;

	public MinimapComponent(Texture minimap, int borderSize)
	{
		this.minimap = minimap;
		this.borderSize = borderSize;
	}

	public Texture getMinimap()
	{
		return minimap;
	}

	public int getBorderSize()
	{
		return borderSize;
	}

	public void setMinimap(Texture minimap)
	{
		this.minimap = minimap;
	}

	public void setBorderSize(int borderSize)
	{
		this.borderSize = borderSize;
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		float scale = getWidth() / minimap.getWidth();
		float scaledBorderSize = borderSize * scale;

		batch.draw(minimap, getX() + scaledBorderSize, getY() + scaledBorderSize, getWidth() - scaledBorderSize * 2, getHeight() - scaledBorderSize * 2);

		super.draw(batch, parentAlpha);
	}

}
