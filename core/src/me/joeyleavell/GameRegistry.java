package me.joeyleavell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import me.joeyleavell.building.BuildingDefinition;
import me.joeyleavell.world.AnimatedTile;
import me.joeyleavell.world.Chunk;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.biome.Biome;
import me.joeyleavell.world.gen.WorldGenerator;
import me.joeyleavell.world.tile.Tile;

public class GameRegistry
{

	private Map<String, Tile> tileMap;
	private Map<String, Biome> biomeMap;
	private List<AnimatedTile> animatedTiles;
	private List<WorldGenerator> worldGenerators;
	private Map<String, BuildingDefinition> buildings;

	public GameRegistry()
	{
		tileMap = new HashMap<String, Tile>();
		biomeMap = new HashMap<String, Biome>();
		worldGenerators = new ArrayList<WorldGenerator>();
		animatedTiles = new ArrayList<AnimatedTile>();
		buildings = new HashMap<String, BuildingDefinition>();
	}

	public void registerTile(Tile t)
	{
		tileMap.put(t.getRegistryName(), t);

		if (t instanceof AnimatedTile)
			animatedTiles.add((AnimatedTile) t);
	}

	public void registerBiome(Biome biome)
	{
		biomeMap.put(biome.getRegistryName(), biome);
	}

	public int getBiomeCount()
	{
		return biomeMap.size();
	}

	public void registerBuilding(BuildingDefinition building)
	{
		buildings.put(building.getRegistryName(), building);
	}

	public void registerWorldGenerator(WorldGenerator generator)
	{
		worldGenerators.add(generator);
	}

	public Tile getTile(String name)
	{
		return tileMap.get(name);
	}

	public Tile getTileById(int id)
	{
		for (String x : tileMap.keySet())
			if (tileMap.get(x).getId() == id)
				return tileMap.get(x);
		return null;
	}

	public Biome getBiome(String name)
	{
		return biomeMap.get(name);
	}

	public Biome getBiomeById(int id)
	{
		for (String biome : biomeMap.keySet())
			if (biomeMap.get(biome).getId() == id)
				return biomeMap.get(biome);
		return null;
	}

	public BuildingDefinition getBuilding(String name)
	{
		return buildings.get(name);
	}

	public Iterator<String> getBuildingNames()
	{
		return buildings.keySet().iterator();
	}

	public Chunk generateChunk(Chunk chunk, Random rand, Location chunkCoord)
	{
		for (WorldGenerator gen : worldGenerators)
			gen.generateChunk(chunk, rand, chunkCoord);

		return chunk;
	}

	public void updateAnimatedTiles()
	{
		for (AnimatedTile tile : animatedTiles)
			tile.nextFrame();
	}

}
