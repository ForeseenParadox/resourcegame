package me.joeyleavell;

import java.io.DataInput;
import java.io.DataOutput;

public interface GameSerializable
{
	
	public void serialize(DataOutput out);
	public void deserialize(DataInput in);

}
