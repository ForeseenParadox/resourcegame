package me.joeyleavell.building;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.joeyleavell.entity.BehaviorVillagerAi;
import me.joeyleavell.entity.DataHouseBuilding;
import me.joeyleavell.entity.Entity;
import me.joeyleavell.entity.EntityFactory;
import me.joeyleavell.world.Location;
import me.joeyleavell.world.MultiTile;
import me.joeyleavell.world.PointLight;
import me.joeyleavell.world.QuadraticAttenuation;
import me.joeyleavell.world.World;
import me.joeyleavell.world.tile.Tile;
import me.joeyleavell.world.tile.TileState;

public class BuildingDefinition
{

	public static final int ENTITY_SPAWN_RADIUS = 5;

	private String registryName;
	private int width;
	private int height;
	private int buildingEntityId;
	private int cost;
	private TileState[] tiles;
	private TextureRegion texture;
	private BuildingLight light;
	private Map<Integer, Integer> entitySpawns;

	public BuildingDefinition(Tile tile, TextureRegion texture, int width, int height, int cost)
	{
		tiles = new TileState[width * height];
		this.width = width;
		this.height = height;
		this.buildingEntityId = -1;
		this.cost = cost;
		this.texture = texture;

		// initialize with tile
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				tiles[y * width + x] = new TileState(tile, y * width + x);

		entitySpawns = new HashMap<Integer, Integer>();
	}

	public BuildingDefinition(TextureRegion texture, int width, int height, int cost)
	{
		tiles = new TileState[width * height];
		this.texture = texture;
		this.width = width;
		this.height = height;
		this.buildingEntityId = -1;
		this.cost = cost;

		entitySpawns = new HashMap<Integer, Integer>();
	}

	public BuildingDefinition addEntitySpawn(int id, int count)
	{
		entitySpawns.put(id, count);

		return this;
	}

	public BuildingDefinition setBuildingEntityId(int buildingEntityId)
	{
		this.buildingEntityId = buildingEntityId;

		return this;
	}

	public boolean canPlace(Location loc)
	{
		return loc.getWorld().validBuildingPlacement(loc.getX(), loc.getY(), width, height);
	}

	public Building makeBuilding(Location loc)
	{
		World w = loc.getWorld();

		// place tiles
		MultiTile m = w.placeBuilding(this, loc);

		// place lights
		PointLight worldLight = null;
		if (light != null)
		{
			Vector2 pos = new Vector2(loc.getX(), loc.getY());
			QuadraticAttenuation atten = light.getAttenuation();
			float inten = light.getIntensity();
			worldLight = new PointLight(pos, Color.WHITE, inten, atten.getQuadratic(), atten.getLinear(), atten.getConstant());
			w.addPointLight(worldLight);
		}

		Building building = new Building(this, m, worldLight);
		Entity buildingEntity = null;
		if (buildingEntityId != -1)
			buildingEntity = EntityFactory.createEntity(w, buildingEntityId);

		if (buildingEntity != null)
		{
			if (buildingEntity.hasDataComponent(DataHouseBuilding.class))
			{
				DataHouseBuilding house = buildingEntity.getDataComponent(DataHouseBuilding.class);
				house.setHouse(building);
			}
			w.addEntity(buildingEntity);
		}

		// place entities
		Set<Integer> entities = getEntitySpawnIds();
		for (int id : entities)
		{
			int count = getEntitySpawnCount(id);
			for (int i = 0; i < count; i++)
			{
				// generate random position
				Location l = generateRandomEntityLoc(building);
				Entity e = EntityFactory.createEntity(w, id, l.getPixelX(), l.getPixelY());
				if (e.hasBehaviorComponent(BehaviorVillagerAi.class))
					e.getBehaviorComponent(BehaviorVillagerAi.class).setHomeEntity(buildingEntity);

				w.addEntity(e);
			}
		}

		return building;
	}

	private Location generateRandomEntityLoc(Building building)
	{
		World w = building.getMultiTile().getLocation().getWorld();
		int randX = 0;
		int randY = 0;
		do
		{
			float theta = (float) (Math.random() * 2 * Math.PI);
			float r = (float) (Math.random() * 2 - 1) * BuildingDefinition.ENTITY_SPAWN_RADIUS;
			randX = (int) (building.getCenterX() + (int) (Math.cos(theta) * r));
			randY = (int) (building.getCenterY() + (int) (Math.sin(theta) * r));
		} while (!w.isTileLocationWalkable(randX, randY));

		return new Location(w, randX, randY);
	}

	public int getEntitySpawnCount(int id)
	{
		if (!entitySpawns.containsKey(id))
			return 0;
		else
			return entitySpawns.get(id);
	}

	public Set<Integer> getEntitySpawnIds()
	{
		return entitySpawns.keySet();
	}

	public TextureRegion getTexture()
	{
		return texture;
	}

	public BuildingDefinition setRegistryName(String registryName)
	{
		this.registryName = registryName;
		return this;
	}

	public BuildingDefinition setLight(BuildingLight light)
	{
		this.light = light;
		return this;
	}

	public BuildingLight getLight()
	{
		return light;
	}

	public String getRegistryName()
	{
		return registryName;
	}

	public TileState[] getBuildingTiles()
	{
		return tiles;
	}

	public TileState getTileState(int x, int y)
	{
		return tiles[y * width + x];
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public int getBuildingEntityId()
	{
		return buildingEntityId;
	}

	public int getCost()
	{
		return cost;
	}

	public void setTile(int x, int y, Tile t, int meta)
	{
		tiles[y * width + x] = new TileState(t, meta);
	}

}
