package me.joeyleavell.building;

import me.joeyleavell.world.Location;
import me.joeyleavell.world.MultiTile;
import me.joeyleavell.world.PointLight;
import me.joeyleavell.world.tile.Tiles;

public class Building
{

	private BuildingDefinition bDef;
	private MultiTile multi;
	private PointLight light;

	public Building(BuildingDefinition bDef, MultiTile m, PointLight worldLight)
	{
		this.bDef = bDef;
		this.multi = m;
		this.light = worldLight;
	}

	public float getCenterX()
	{
		return multi.getLocation().getX() + bDef.getWidth() / 2f;
	}

	public float getCenterY()
	{
		return multi.getLocation().getY() + bDef.getHeight() / 2f;
	}

	public BuildingDefinition getBuildingDef()
	{
		return bDef;
	}

	public MultiTile getMultiTile()
	{
		return multi;
	}

	public int getWidth()
	{
		return bDef.getWidth();
	}

	public int getHeight()
	{
		return bDef.getHeight();
	}

	public boolean isLocationInBuilding(Location l)
	{
		Location loc = multi.getLocation();
		return l.getX() >= loc.getX() && l.getX() < loc.getX() + bDef.getWidth() && l.getY() >= loc.getY() && l.getY() < loc.getY() + bDef.getHeight();
	}

	public void destroy()
	{
		Location loc = multi.getLocation();
		// iterate through all building tiles and destroy them
		for (int i = 0; i < bDef.getWidth(); i++)
			for (int j = 0; j < bDef.getHeight(); j++)
				loc.getWorld().getForegroundTileState(loc.getX() + i, loc.getY() + j).setTile(Tiles.AIR);

		// remove light
		loc.getWorld().removePointLight(light);
	}

}
