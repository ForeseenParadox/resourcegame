package me.joeyleavell.building;

import me.joeyleavell.world.Location;
import me.joeyleavell.world.QuadraticAttenuation;

public class BuildingLight
{

	private float intensity;
	private QuadraticAttenuation atten;

	public BuildingLight(float intensity, QuadraticAttenuation atten)
	{
		this.intensity = intensity;
		this.atten = atten;
	}

	public float getIntensity()
	{
		return intensity;
	}

	public QuadraticAttenuation getAttenuation()
	{
		return atten;
	}

}
