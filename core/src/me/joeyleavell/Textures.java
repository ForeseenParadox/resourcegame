package me.joeyleavell;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Textures
{

	public static final TextureRegion VOID = new TextureRegion(Game.getTileSheet(), 0 * 4, 0 * 4, 4, 4);
	public static final TextureRegion LIGHT_GRASS = new TextureRegion(Game.getTileSheet(), 1 * 4, 0 * 4, 4, 4);
	public static final TextureRegion DARK_GRASS = new TextureRegion(Game.getTileSheet(), 1 * 4, 1 * 4, 4, 4);
	public static final TextureRegion DIRT = new TextureRegion(Game.getTileSheet(), 2 * 4, 0 * 4, 4, 4);
	public static final TextureRegion DARK_DIRT = new TextureRegion(Game.getTileSheet(), 2 * 4, 1 * 4, 4, 4);
	public static final TextureRegion SAND = new TextureRegion(Game.getTileSheet(), 3 * 4, 0 * 4, 4, 4);
	public static final TextureRegion SAND_STONE = new TextureRegion(Game.getTileSheet(), 3 * 4, 1 * 4, 4, 4);
	public static final TextureRegion LIGHT_STONE = new TextureRegion(Game.getTileSheet(), 4 * 4, 0 * 4, 4, 4);
	public static final TextureRegion DARK_STONE = new TextureRegion(Game.getTileSheet(), 4 * 4, 1 * 4, 4, 4);
	public static final TextureRegion SNOW = new TextureRegion(Game.getTileSheet(), 5 * 4, 0 * 4, 4, 4);
	public static final TextureRegion DARK_SNOW = new TextureRegion(Game.getTileSheet(), 5 * 4, 1 * 4, 4, 4);
	public static final TextureRegion WATER_1 = new TextureRegion(Game.getTileSheet(), 6 * 4, 0 * 4, 4, 4);
	public static final TextureRegion WATER_2 = new TextureRegion(Game.getTileSheet(), 6 * 4, 1 * 4, 4, 4);
	public static final TextureRegion LAVA_1 = new TextureRegion(Game.getTileSheet(), 7 * 4, 0 * 4, 4, 4);
	public static final TextureRegion LAVA_2 = new TextureRegion(Game.getTileSheet(), 7 * 4, 1 * 4, 4, 4);
	public static final TextureRegion CAMPFIRE_1 = new TextureRegion(Game.getTileSheet(), 13 * 4, 15 * 4, 4, 4);
	public static final TextureRegion CAMPFIRE_2 = new TextureRegion(Game.getTileSheet(), 14 * 4, 15 * 4, 4, 4);
	public static final TextureRegion CAMPFIRE_3 = new TextureRegion(Game.getTileSheet(), 15 * 4, 15 * 4, 4, 4);
	public static final TextureRegion BOULDER_1 = new TextureRegion(Game.getTileSheet(), 9 * 4, 0 * 4, 4, 4);
	public static final TextureRegion BOULDER_2 = new TextureRegion(Game.getTileSheet(), 9 * 4, 1 * 4, 4, 4);

	public static final TextureRegion SMALL_HOUSE = new TextureRegion(Game.getTileSheet(), 3 * 4, 14 * 4, 2 * 4, 2 * 4);
	public static final TextureRegion MAGIC_TOWER = new TextureRegion(Game.getTileSheet(), 3 * 4, 11 * 4, 2 * 4, 3 * 4);
	public static final TextureRegion BIG_HOUSE = new TextureRegion(Game.getTileSheet(), 0 * 4, 14 * 4, 3 * 4, 2 * 4);
	public static final TextureRegion BARRACKS = new TextureRegion(Game.getTileSheet(), 0 * 4, 12 * 4, 3 * 4, 2 * 4);
	public static final TextureRegion FLAG_POLE_1 = new TextureRegion(Game.getTileSheet(), 13 * 4, 12 * 4, 4, 3 * 4);
	public static final TextureRegion FLAG_POLE_2 = new TextureRegion(Game.getTileSheet(), 14 * 4, 12 * 4, 4, 3 * 4);
	public static final TextureRegion FLAG_POLE_3 = new TextureRegion(Game.getTileSheet(), 15 * 4, 12 * 4, 4, 3 * 4);
	public static final TextureRegion FLAG_POLE_BOTTOM = new TextureRegion(Game.getTileSheet(), 13 * 4, 14 * 4, 4, 4);
	public static final TextureRegion FLAG_MIDDLE_1 = new TextureRegion(Game.getTileSheet(), 13 * 4, 13 * 4, 4, 4);
	public static final TextureRegion FLAG_MIDDLE_2 = new TextureRegion(Game.getTileSheet(), 14 * 4, 13 * 4, 4, 4);
	public static final TextureRegion FLAG_MIDDLE_3 = new TextureRegion(Game.getTileSheet(), 15 * 4, 13 * 4, 4, 4);
	public static final TextureRegion FLAG_TOP_1 = new TextureRegion(Game.getTileSheet(), 13 * 4, 12 * 4, 4, 4);
	public static final TextureRegion FLAG_TOP_2 = new TextureRegion(Game.getTileSheet(), 14 * 4, 12 * 4, 4, 4);
	public static final TextureRegion FLAG_TOP_3 = new TextureRegion(Game.getTileSheet(), 15 * 4, 12 * 4, 4, 4);

	public static final TextureRegion FARM_1 = new TextureRegion(Game.getTileSheet(), 12 * 4, 15 * 4, 4, 4);
	public static final TextureRegion FARM_2 = new TextureRegion(Game.getTileSheet(), 11 * 4, 15 * 4, 4, 4);
	public static final TextureRegion FARM_3 = new TextureRegion(Game.getTileSheet(), 10 * 4, 15 * 4, 4, 4);
	public static final TextureRegion FARM_4 = new TextureRegion(Game.getTileSheet(), 9 * 4, 15 * 4, 4, 4);

	public static final TextureRegion TREE_1 = new TextureRegion(Game.getTileSheet(), 12 * 4, 0 * 4, 8, 8);
	public static final TextureRegion TREE_2 = new TextureRegion(Game.getTileSheet(), 14 * 4, 6 * 4, 8, 8);
	public static final TextureRegion SHRUB_1 = new TextureRegion(Game.getTileSheet(), 11 * 4, 0 * 4, 4, 4);
	public static final TextureRegion SHRUB_2 = new TextureRegion(Game.getTileSheet(), 11 * 4, 1 * 4, 4, 4);
	public static final TextureRegion CACTUS_1 = new TextureRegion(Game.getTileSheet(), 10 * 4, 2 * 4, 4, 4);
	public static final TextureRegion CACTUS_2 = new TextureRegion(Game.getTileSheet(), 11 * 4, 2 * 4, 4, 8);
	public static final TextureRegion CACTUS_3 = new TextureRegion(Game.getTileSheet(), 12 * 4, 2 * 4, 8, 8);
	public static final TextureRegion CACTUS_4 = new TextureRegion(Game.getTileSheet(), 14 * 4, 2 * 4, 8, 8);

	// Entities
	public static final TextureRegion BLUE_BIRD = new TextureRegion(Game.getEntitySheet(), 26, 0, 6, 2);

	public static final TextureRegion VILLAGER_1 = new TextureRegion(Game.getEntitySheet(), 0 * 4, 1 * 4, 4, 4);
	public static final TextureRegion VILLAGER_2 = new TextureRegion(Game.getEntitySheet(), 1 * 4, 1 * 4, 4, 4);
	public static final TextureRegion VILLAGER_3 = new TextureRegion(Game.getEntitySheet(), 2 * 4, 1 * 4, 4, 4);

	public static final TextureRegion BUILDER_1 = new TextureRegion(Game.getEntitySheet(), 0 * 4, 5 * 4, 4, 4);
	public static final TextureRegion BUILDER_2 = new TextureRegion(Game.getEntitySheet(), 1 * 4, 5 * 4, 4, 4);
	public static final TextureRegion BUILDER_3 = new TextureRegion(Game.getEntitySheet(), 2 * 4, 5 * 4, 4, 4);

	// Particles
	public static final TextureRegion RAIN_TRAIL = new TextureRegion(Game.getParticleSheet(), 0, 0, 2, 7 * 4);
	public static final TextureRegion RAIN_HEAD = new TextureRegion(Game.getParticleSheet(), 0, 7 * 4, 2, 4);

}
