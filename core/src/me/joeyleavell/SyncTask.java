package me.joeyleavell;

public class SyncTask
{

	private int delay;
	private Runnable runnable;
	private long lastInvocation;

	public SyncTask(int delay, Runnable runnable)
	{
		this.delay = delay;
		this.runnable = runnable;
	}

	public int getDelay()
	{
		return delay;
	}

	public long getLastInvocation()
	{
		return lastInvocation;
	}

	public void setDelay(int delay)
	{
		this.delay = delay;
	}

	public boolean ready()
	{
		return System.currentTimeMillis() - lastInvocation >= delay;
	}

	public void invoke()
	{
		runnable.run();
		lastInvocation = System.currentTimeMillis();
	}

}
