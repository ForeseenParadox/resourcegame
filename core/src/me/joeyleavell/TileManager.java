package me.joeyleavell;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import me.joeyleavell.world.Material;

public class TileManager
{

	private Map<Integer, Drawable> tileTextures = new HashMap<Integer, Drawable>();
	private Map<Integer, Material> tileMaterials = new HashMap<Integer, Material>();

	public TileManager()
	{
		tileTextures = new HashMap<Integer, Drawable>();
	}

	public Drawable getTileDrawable(int id)
	{
		return tileTextures.get(id);
	}

	public void register(int id, Drawable drawable, int tilesWide, int tilesTall, Material mat)
	{
		tileTextures.put(id, drawable);
		tileMaterials.put(id, mat);
	}

}
